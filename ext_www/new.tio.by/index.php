<?define("HOME_PAGE", true);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("keywords", "тио бай, tio.by, туризм и отдых");
$APPLICATION->SetPageProperty("description", "ТИО БАЙ (tio.by) - интернет-портал \"Туризм и Отдых\". Каталог туров, акций и спецпредложений!");
$APPLICATION->SetTitle("Интернет-портал TIO.BY | Туризм и Отдых | ТИО БАЙ");
?>


    <div class="ts-wrap ts-my-10">
        <section class="ts-row">
            <div class="ts-col-24 ts-col-lg-16">
                <?    CModule::IncludeModule("iblock");
                global $newsFilter;
                $arFilterArticles = Array("IBLOCK_ID"=>2, "PROPERTY_MAIN_VALUE"=>'1', "ACTIVE"=>"Y" );
                ?>
                <?$APPLICATION->IncludeComponent(
                    "travelsoft:travelsoft.iblock.getlist.byfilter",
                    "home_articles",
                    Array(
                        "CACHE_TIME" => "0",
                        "CACHE_TYPE" => "A",
                        "CNT" => "4",
                        "FILTER_NAME" => "arFilterArticles",
                        "SELECT" => array("NAME", "ID", "SHOW_COUNTER", "PREVIEW_TEXT", "DETAIL_TEXT", "DETAIL_PAGE_URL", "ACTIVE_FROM", "DATE_CREATE", "PROPERTY_MORE_PHOTO", "PROPERTY_AUTHOR", "PROPERTY_MAIN"),
                        "ORDER" => "ASC",
                        "SORT" => "SORT",
                        "TITLE" => "",
                        "DESCRIPTION_LINK" => "",
                        "DESCRIPTION_NAMELINK" => "",
                        "TEXT_DESCRIPTION" => "",
                        "TEXT_TITLE" => "",
                        "SHOW_PROPERTIES" => "Y",
                        "ACTIVE_DATE_FORMAT" => "M j, Y",
                    )
                );
                ?>
            </div>

            <div class="ts-col-24 ts-col-lg-8">
                <?$GLOBALS["arFilterNews"] = ARRAY("IBLOCK_ID" => "2", "!ID" => $GLOBALS["TOP_NEWS_ELEMENTS_ID"], "SECTION_ID" => 94, "ACTIVE"=>"Y");?>
                <?$APPLICATION->IncludeComponent(
                    "travelsoft:travelsoft.iblock.getlist.byfilter",
                    "home_news",
                    Array(
                        "CACHE_TIME" => "0",
                        "CACHE_TYPE" => "A",
                        "CNT" => "5",
                        "FILTER_NAME" => "arFilterNews",
                        "SELECT" => array("NAME", "ID", "SHOW_COUNTER", "DETAIL_PAGE_URL", "MORE_PHOTO", "PROPERTY_PHOTO1", "PROPERTY_PHOTO2", "PROPERTY_PHOTO3", "PROPERTY_PHOTO4", "PROPERTY_OLD_PHOTO",
                            "PROPERTY_FORUM_MESSAGE_CNT", "PROPERTY_THEME", "ACTIVE_FROM", "DATE_CREATE"),
                        "ORDER" => "DESC",
                        "SORT" => "DATE_ACTIVE_FROM",
                        "TITLE" => "",
                        "DESCRIPTION_LINK" => "",
                        "DESCRIPTION_NAMELINK" => "",
                        "TEXT_DESCRIPTION" => "",
                        "TEXT_TITLE" => "Новости",
                        "SHOW_PROPERTIES" => "Y",
                        "ACTIVE_DATE_FORMAT" => "M j, Y",
                    )
                );
                ?>
                <? $APPLICATION->IncludeComponent(
                    "asd:subscribe.quick.form",
                    "home",
                    array(
                        "FORMAT" => "html",
                        "INC_JQUERY" => "N",
                        "NOT_CONFIRM" => "N",
                        "RUBRICS" => array(
                            0 => "1",
                        ),
                        "SHOW_RUBRICS" => "N",
                        "COMPONENT_TEMPLATE" => "home",
                        "COMPOSITE_FRAME_MODE" => "A",
                        "COMPOSITE_FRAME_TYPE" => "AUTO"
                    ),
                    false
                ); ?>
            </div>
        </section>
    </div>

    <section class="ts-my-10">
        <?$APPLICATION->IncludeComponent(
            "travelsoft:travelsoft.news.list",
            "home_shares",
            array(
                "ACTIVE_DATE_FORMAT" => "d.m.Y",
                "ADD_SECTIONS_CHAIN" => "N",
                "AJAX_MODE" => "N",
                "AJAX_OPTION_ADDITIONAL" => "",
                "AJAX_OPTION_HISTORY" => "N",
                "AJAX_OPTION_JUMP" => "N",
                "AJAX_OPTION_STYLE" => "N",
                "CACHE_FILTER" => "N",
                "CACHE_GROUPS" => "N",
                "CACHE_TIME" => "36000000",
                "CACHE_TYPE" => "A",
                "CHECK_DATES" => "Y",
                "COMPONENT_TEMPLATE" => "home_shares",
                "DESCRIPTION_LINK" => "",
                "DESCRIPTION_NAMELINK" => "",
                "DETAIL_URL" => "",
                "DISPLAY_BOTTOM_PAGER" => "N",
                "DISPLAY_DATE" => "Y",
                "DISPLAY_NAME" => "Y",
                "DISPLAY_PICTURE" => "Y",
                "DISPLAY_PREVIEW_TEXT" => "Y",
                "DISPLAY_TOP_PAGER" => "N",
                "FIELD_CODE" => array(
                    0 => "",
                    1 => "",
                ),
                "FILTER_NAME" => "arFilterShares",
                "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                "IBLOCK_ID" => "38",
                "IBLOCK_TYPE" => "news",
                "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                "INCLUDE_SUBSECTIONS" => "N",
                "MESSAGE_404" => "",
                "NEWS_COUNT" => "7",
                "PAGER_BASE_LINK_ENABLE" => "N",
                "PAGER_DESC_NUMBERING" => "N",
                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                "PAGER_SHOW_ALL" => "N",
                "PAGER_SHOW_ALWAYS" => "N",
                "PAGER_TEMPLATE" => ".default",
                "PAGER_TITLE" => "",
                "PARENT_SECTION" => "",
                "PARENT_SECTION_CODE" => "",
                "PREVIEW_TRUNCATE_LEN" => "",
                "PROPERTY_CODE" => array(
                    0 => "DATE",
                    1 => "ADDRESS",
                    2 => "TYPE",
                    3 => "DISCOUNT",
                    4 => "PHOTO",
                    5 => "CURRENCY",
                    6 => "COUNTRY",
                    7 => "",
                ),
                "SET_BROWSER_TITLE" => "N",
                "SET_LAST_MODIFIED" => "N",
                "SET_META_DESCRIPTION" => "N",
                "SET_META_KEYWORDS" => "N",
                "SET_STATUS_404" => "N",
                "SET_TITLE" => "N",
                "SHOW_404" => "N",
                "SORT_BY1" => "ACTIVE_FROM",
                "SORT_BY2" => "ID",
                "SORT_ORDER1" => "DESC",
                "SORT_ORDER2" => "ASC",
                "TEXT_DESCRIPTION" => "",
                "TEXT_TITLE" => "Афиша",
                "COMPOSITE_FRAME_MODE" => "A",
                "COMPOSITE_FRAME_TYPE" => "AUTO",
                "AFP_ID" => "",
                "AFP_316" => array(
                )
            ),
            false
        );?>
    </section>
    <!--
    <div class="bg-blue ts-my-10 ts-py-10">
        <div class="ts-wrap ts-px-4 ">
            <div class="widget-title ui-title-inner ui-title-inner_lg">
                <span class="ui-title-inner__inner">Календарь туров</span>
            </div>
            <div class="posts-group-4 tv-calendar tv-moduleid-965071"></div>
            <script type="text/javascript" src="//tourvisor.ru/module/init.js"></script>
        </div>
    </div> -->
    <div class="bg-grey-1 ts-my-10">
        <div class="ts-wrap">
            <?$GLOBALS["arFilterAations"] = array("IBLOCK_ID" => array("23"), "ACTIVE"=>"Y");
            $APPLICATION->IncludeComponent(
                "travelsoft:travelsoft.iblock.getlist.byfilter",
                "home_aktsii-skidki",
                Array(
                    "CACHE_TIME" => "0",
                    "CACHE_TYPE" => "A",
                    "CNT" => "4",
                    "FILTER_NAME" => "arFilterAations",
                    "SELECT" => array("NAME", "ID", "SHOW_COUNTER", "DETAIL_PAGE_URL", "ACTIVE_FROM", "DATE_CREATE", "PROPERTY_PHOTO", "PROPERTY_TEXT", "PROPERTY_TEXT_PREVIEW"),
                    "ORDER" => "DESC",
                    "SORT" => "ACTIVE_FROM",
                    "TITLE" => "",
                    "DESCRIPTION_LINK" => "",
                    "DESCRIPTION_NAMELINK" => "",
                    "TEXT_DESCRIPTION" => "",
                    "TEXT_TITLE" => "Акции и скидки",
                    "SHOW_PROPERTIES" => "Y",
                    "ACTIVE_DATE_FORMAT" => "M j, Y",
                )
            );
            ?>
        </div>
    </div>
    <div class="bg-grey-2 ts-my-10 ts-py-10">
        <div class="ts-wrap">
            <?$GLOBALS["arFilterTourBusiness"] = array("IBLOCK_ID" => array("36","2"), "ACTIVE"=>"Y", "PROPERTY_TYPE_PUBLICATION"=>61745);
            $APPLICATION->IncludeComponent(
                "travelsoft:travelsoft.iblock.getlist.byfilter",
                "home_tour_business",
                Array(
                    "CACHE_TIME" => "0",
                    "CACHE_TYPE" => "A",
                    "CNT" => "3",
                    "FILTER_NAME" => "arFilterTourBusiness",
                    "SELECT" => array("NAME", "ID", "SHOW_COUNTER", "PREVIEW_TEXT", "DETAIL_TEXT", "DETAIL_PAGE_URL", "ACTIVE_FROM", "DATE_CREATE", "PROPERTY_MORE_PHOTO", "PROPERTY_AUTHOR", "PROPERTY_PHOTO3"),
                    "ORDER" => "DESC",
                    "SORT" => "ACTIVE_FROM",
                    "TITLE" => "",
                    "DESCRIPTION_LINK" => "",
                    "DESCRIPTION_NAMELINK" => "",
                    "TEXT_DESCRIPTION" => "",
                    "TEXT_TITLE" => "Статьи",
                    "SHOW_PROPERTIES" => "Y",
                    "ACTIVE_DATE_FORMAT" => "M j, Y",
                )
            );
            ?>
        </div>
    </div>
    <div class="ts-my-10">
        <div class="ts-wrap">
            <?$GLOBALS["arFilterTourBusiness"] = array("IBLOCK_ID" => array("36","2"), "ACTIVE"=>"Y", "PROPERTY_THEME"=>58880);
            $APPLICATION->IncludeComponent(
                "travelsoft:travelsoft.iblock.getlist.byfilter",
                "home_tour_business",
                Array(
                    "CACHE_TIME" => "0",
                    "CACHE_TYPE" => "A",
                    "CNT" => "3",
                    "FILTER_NAME" => "arFilterTourBusiness",
                    "SELECT" => array("NAME", "ID", "SHOW_COUNTER", "PREVIEW_TEXT", "DETAIL_TEXT", "DETAIL_PAGE_URL", "ACTIVE_FROM", "DATE_CREATE", "PROPERTY_MORE_PHOTO", "PROPERTY_AUTHOR", "PROPERTY_PHOTO3"),
                    "ORDER" => "DESC",
                    "SORT" => "ACTIVE_FROM",
                    "TITLE" => "",
                    "DESCRIPTION_LINK" => "",
                    "DESCRIPTION_NAMELINK" => "",
                    "TEXT_DESCRIPTION" => "",
                    "TEXT_TITLE" => "Турбизнес",
                    "SHOW_PROPERTIES" => "Y",
                    "ACTIVE_DATE_FORMAT" => "M j, Y",
                )
            );
            ?>
        </div>
    </div>


<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>