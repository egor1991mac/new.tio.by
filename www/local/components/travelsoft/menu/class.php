<?php 
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

/*
 * make static menu
 */

class StaticMenu
	extends CBitrixComponent {
	
	protected $id = null;

	protected function prepare() {

		$this->id = (int)$this->arParams['ID'];

		return $this->id > 0 ? true : false;

	}
	
	public function executeComponent() {

		if (!$this->prepare())
			return false;
		
		$menuPoints = @include __DIR__ . '/default.menu.php';

		if (empty($menuPoints))
			return false;

		\Bitrix\Main\Loader::includeModule('iblock');		

		$result = array();

		foreach ($menuPoints as $point) {

			$show = true; $method = (string)$point['method'];
			if ($method != "") {
				if (!method_exists($this, $method) || call_user_func_array(array($this, $method))) {
					$show = false; 
				}
			}

			if ($show) 
				$result[] = array('name' => $point['name'], 'link' => $point['link']);	
			
		}

		$this->arResult = $result;

		$this->IncludeComponentTemplate();
			
	}

	protected function elEx(array $filter) {
	
		if ($filter['IBLOCK_ID'] <= 0)
			return false;
		
		$r = CIBlockElement::GetList(
			false,
			$filter,
			flase,
			array('nTopCount' => 1),
			array('ID')		
		)->Fetch();

		return $r['ID'] > 0 ? true : false; 

	}

	protected function haveCities() {
	
		return $this->elEx(array('IBLOCK_ID' => 11, 'PROPERTY_COUNTRY' => $this->id));
	
	}

	protected function haveSights() {
	
		if (!$this->elEx(array('IBLOCK_ID' => 18, 'PROPERTY_COUNTRY' => $this->id)))
			if (!$this->elEx(array('IBLOCK_ID' => 18, 'PROPERTY_CITY' => $this->id)))
				return false;
		
		return true;
	
	}

	protected function haveNews() {
	
		if (!$this->elEx(array('IBLOCK_ID' => 2, 'PROPERTY_COUNTRY' => $this->id)))
			if (!$this->elEx(array('IBLOCK_ID' => 2, 'PROPERTY_RESORT' => $this->id)))
				return false;
		
		return true;
	
	}

	protected function haveHotels() {

		return false;	
	
	}

	protected function haveExcursions() {

		return false;	
	
	}

	protected function haveVisa() {

		return false;	
	
	}

	protected function haveReviews() {

		return false;	
	
	}

	protected function haveTours() {

		return false;	
	
	}

	protected function haveFlights() {

		return false;	
	
	}
	
}
