<?

class TravelsoftSearchInfoblockItems extends CBitrixComponent
{

    public function executeComponent()
    {
        $iblock_type = $this->arParams['IBLOCK_TYPE'];
        $iblock_id = $this->arParams['IBLOCK_ID'];

        $res = CIBlockElement::GetList(Array(), Array("IBLOCK_ID" => $iblock_id, "IBLOCK_TYPE" => $iblock_type, "ACTIVE" => "Y"), false, false, Array("IBLOCK_ID", "ID", "NAME", "CODE", "DETAIL_PAGE_URL"));

        $items = [];
        $sources = [];
        while ($ob = $res->GetNextElement()) {
            $arFields = $ob->GetFields();

            $item = [
                'ID' => $arFields['ID'],
                'NAME' => $arFields['NAME'],
                'DETAIL_PAGE_URL' => $arFields['DETAIL_PAGE_URL'],
            ];

            $source= [
                "label" => $item['NAME'],
                "value" => $item['NAME'],
                "id" => $item['ID'],
            ];

            $items[$item['ID']] = $item;
            $sources[] = $source;
        }

        $this->arResult['ITEMS'] = $items;
        $this->arResult['SOURCE'] = $sources;

        $this->IncludeComponentTemplate();

    }

}
