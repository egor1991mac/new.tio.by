<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
$this->setFrameMode(false);
?>
<?
$this->addExternalCss(SITE_TEMPLATE_PATH . "/assets/css/jquery-ui.min.css");
$this->addExternalJS(SITE_TEMPLATE_PATH . "/assets/owl/jquery-ui-autocomplete.min.owl");
?>

<div class="row">
    <div class="col-xs-12">
        <h4 class="typography-title">Поиск</h4>
        <div class="row">
            <div class="col-md-9">
                <form class="ui-form ui-form-1" id="search__by__name" action="">
                    <div class="row">
                        <div class="col-md-6">
                            <input id="search__by__name_input" class="form-control" type="text" placeholder="Поиск"/>
                            <div id="menu-container" style="position:absolute;"></div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script>
    (function ($) {

        let highlight = function (ul, it) {
            let v = $(this.element[0]).val(), w = it.label;
            if (v !== '')
                w = w.replace(new RegExp("(" + $.ui.autocomplete.escapeRegex(v) + ")", "ig"), "<strong>$1</strong>");
            return $("<li></li>")
                .data("ui-autocomplete-item", it)
                .append(w)
                .appendTo(ul);
        };

        let data = <?= \Bitrix\Main\Web\Json::encode($arResult['ITEMS'])?>;
        $("#search__by__name_input").autocomplete({
            source: <?= \Bitrix\Main\Web\Json::encode($arResult['SOURCE'])?>,
            select: function (event, ui) {
                let form = $("#search__by__name");
                if (data[ui.item.id].DETAIL_PAGE_URL) {
                    form.attr("action", data[ui.item.id].DETAIL_PAGE_URL);

                    form.submit();
                }
                else {
                    return false;
                }
            },
            appendTo: '#menu-container'
        }).on('focus', function () {
            $(this).autocomplete('search');
        }).data("ui-autocomplete")._renderItem = highlight;

    })(jQuery);
</script>