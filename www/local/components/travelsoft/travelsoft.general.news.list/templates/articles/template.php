<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<?if(!empty($arResult["ITEMS"])):?>

    <div class="l-main-content l-main-content_mrg-right_minus">
    
        <div class="posts-group">
        <?$i = 1;?>

        <?foreach($arResult["ITEMS"] as $arItem):?>
            <?
            $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
            $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
            $img = getSrc($arItem["DISPLAY_PROPERTIES"]["MORE_PHOTO"]["VALUE"], array('width' => 360, 'height' => 220));
            if(empty($img[0])){
                $img = getSrc($arItem["DISPLAY_PROPERTIES"]["PHOTO3"]["VALUE"], array('width' => 360, 'height' => 220), NO_PHOTO_PATH_360_220);
            }?>


            <?if($i == 1 || ($i - 1) % 3 == 0):?>

                <section class="b-post ts-mb-6 b-post_img-left b-post_img-360 b-post_img-360_mod-a b-post_bg-blue clearfix" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
                   <div class="ts-row ">
                    <div class="entry-media ts-col-24 ts-col-sm-12 ">
                        <a class="ts-mx-0 ts-width-100" href="<?=$arItem["DETAIL_PAGE_URL"]?>">
                            <img class="ts-width-100" src="<?=$img[0]?>" alt="<?=$arItem["NAME"]?>">
                        </a>
                    </div>
                    <div class="entry-main ts-p-4 ts-px-sm-2 ts-col-24 ts-col-sm-12">
                        <div class="entry-header">
                            <div class="entry-meta">
                                <?if(in_array(array(1,7),$GLOBALS["USER"]->GetUserGroupArray())):?>
                                    <span class="entry-meta__item"><i class="icon fa fa-heart-o text-second"></i>
                                        <a class="entry-meta__link text-primary" href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=$arItem["SHOW_COUNTER"]?></a>
                                    </span>
                                <?endif?>
                                <!--<span class="entry-meta__item"><i class="icon fa fa-comment-o text-primary"></i>
                                    <a class="entry-meta__link text-primary" href="blog-main.html">29</a>
                                </span>-->
                            </div>
                            <h4 class="entry-title"><a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=$arItem["NAME"]?></a></h4>
                        </div>
                        <div class="entry-content">
                            <p>
                                <?if(!empty($arItem["PREVIEW_TEXT"])):?>
                                    <?=substr2($arItem["~PREVIEW_TEXT"], 120)?>
                                <?elseif(!empty($arItem["DETAIL_TEXT"])):?>
                                    <?=substr2($arItem["~DETAIL_TEXT"], 120)?>
                                <?endif?>
                            </p>
                        </div>
                        <div class="entry-footer"><a class="btn-bd-primary" href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=GetMessage('NEWS_MORE_INFO')?></a></div>
                    </div>
                   </div>
                </section>

            <?else:?>

                <?if(($i + 1) % 3 == 0):?>
                    <div class="posts-group-2 section-area ts-pt-0 ">
                        <div class="ts-row">
                <?endif;?>

                <div class="ts-col-24 ts-col-sm-12 ts-pb-6" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
                    <section class="b-post ">
                        <div class="entry-media">
                            <a class=" ts-width-100" href="<?=$arItem["DETAIL_PAGE_URL"]?>">
                                <img class="ts-width-100" src="<?=$img[0]?>" alt="<?=$arItem["NAME"]?>">
                            </a>
                        </div>
                        <div class="entry-main">
                            <div class="entry-header">
                                <div class="entry-meta">
                                    <?if(in_array(array(1,7),$GLOBALS["USER"]->GetUserGroupArray())):?>
                                        <span class="entry-meta__item"><i class="icon fa fa-heart-o text-second"></i>
                                            <a class="entry-meta__link text-primary" href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=$arResult["SHOW_COUNTER"]?></a>
                                        </span>
                                    <?endif?>
                                    <!--<span class="entry-meta__item"><i class="icon fa fa-comment-o text-primary"></i>
                                        <a class="entry-meta__link text-primary" href="blog-main.html">29</a>
                                    </span>-->
                                </div>
                                <h4 class="entry-title ts-px-0"><a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=$arItem["NAME"]?></a></h4>
                            </div>
                            <div class="entry-content">
                                <p>
                                    <?if(!empty($arItem["PREVIEW_TEXT"])):?>
                                        <?=substr2($arItem["~PREVIEW_TEXT"], 120)?>
                                    <?elseif(!empty($arItem["DETAIL_TEXT"])):?>
                                        <?=substr2($arItem["~DETAIL_TEXT"], 120)?>
                                    <?endif?>
                                </p>
                            </div>
                        </div>
                    </section>
                </div>

                <?if($i % 3 == 0 || $i == (count($arResult["ITEMS"]) + 1)):?>
                        </div>
                    </div>
                <?endif?>

            <?endif?>

            <?$i++?>

        <?endforeach;?>

        </div>
    </div>
    <div class="ts-row ts-justify-content__center ts-mb-6">
    <?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
        <?=$arResult["NAV_STRING"]?>
    <?endif;?>
    </div>
<?endif?>

