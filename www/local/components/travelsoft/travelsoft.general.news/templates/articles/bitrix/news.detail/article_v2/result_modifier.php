<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

if(!empty($arResult["DISPLAY_PROPERTIES"]["AUTHOR"]["VALUE"])){

    $rsUser = CUser::GetByID($arResult["DISPLAY_PROPERTIES"]["AUTHOR"]["VALUE"]);
    $arUser = $rsUser->Fetch();
    $arResult["DISPLAY_PROPERTIES"]["AUTHOR"]["DESC"] = $arUser["NAME"]." ".$arUser["LAST_NAME"];

}

if(!empty($arResult["DISPLAY_PROPERTIES"]["MORE_PHOTO"]["VALUE"]) && count($arResult["DISPLAY_PROPERTIES"]["MORE_PHOTO"]["VALUE"]) > 1){

    $arResult["SLIDER1"] =  getSrc($arResult["DISPLAY_PROPERTIES"]["MORE_PHOTO"]["VALUE"], array('width' => 740, 'height' => 533));

}

if(!empty($arResult["DISPLAY_PROPERTIES"]["MORE_PHOTO2"]["VALUE"]) && count($arResult["DISPLAY_PROPERTIES"]["MORE_PHOTO2"]["VALUE"]) > 1){

    $arResult["SLIDER2"] =  getSrc($arResult["DISPLAY_PROPERTIES"]["MORE_PHOTO2"]["VALUE"], array('width' => 740, 'height' => 533));

}