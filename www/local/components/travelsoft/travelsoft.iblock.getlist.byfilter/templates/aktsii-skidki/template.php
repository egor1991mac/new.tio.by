<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
   /** @var array $arParams */
   /** @var array $arResult */
   /** @global CMain $APPLICATION */
   /** @global CUser $USER */
   /** @global CDatabase $DB */
   /** @var CBitrixComponentTemplate $this */
   /** @var string $templateName */
   /** @var string $templateFile */
   /** @var string $templateFolder */
   /** @var string $componentPath */
   /** @var CBitrixComponent $component */
   $this->setFrameMode(true);
   $i=0;
   ?>
<ul class="row-widget-list">
   <?foreach($arResult as $arItem):?>
   <?
      $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
      $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
      ?>
	<?if (!empty($arItem["PROPERTIES"]["PHOTO"]["VALUE"][0])):
	$file = CFile::ResizeImageGet($arItem["PROPERTIES"]["PHOTO"]["VALUE"][0], array('width'=>322, 'height'=>180), BX_RESIZE_IMAGE_EXACT, true);
	$pre_photo=$file["src"];
	elseif (!empty($arItem["PROPERTY_PHOTO_VALUE"][0])):
	$file = CFile::ResizeImageGet($arItem["PROPERTY_PHOTO_VALUE"][0], array('width'=>322, 'height'=>180), BX_RESIZE_IMAGE_EXACT, true);
	$pre_photo=$file["src"];
    elseif (!empty($arItem["PREVIEW_PICTURE"])):
	$an_file = CFile::ResizeImageGet($arItem["PREVIEW_PICTURE"], array('width'=>322, 'height'=>180), BX_RESIZE_IMAGE_EXACT, true);
	//print_r ($an_file);
	$pre_photo=$an_file["src"];
	elseif (!empty($arItem["PROPERTY_OLD_PHOTO_VALUE"])):
	$pre_photo="/datas/photos/".$arItem["PROPERTY_OLD_PHOTO_VALUE"];
	else:
	$pre_photo=SITE_TEMPLATE_PATH."/images/nophoto.jpg";
	endif;
	?>

   <?$i++;?>
   <li  id="<?=$this->GetEditAreaId($arItem['ID']);?>">
      <a href="<?=$arItem["DETAIL_PAGE_URL"]?>" rel="bookmark">
         <div class="row-widget-img left relative">
			 <img class="archive-list-img left relative" data-aload="<?=$pre_photo?>" alt="<?echo $arItem["NAME"]?>" >

            <div class="feat-info-wrap">
               <?if (!empty($arItem["SHOW_COUNTER"])):?>
               <div class="feat-info-views">
                  <i class="fa fa-eye fa-2"></i> <span class="feat-info-text"><?=$arItem["SHOW_COUNTER"]?></span>
               </div>
               <!--feat-info-views-->
               <?endif;?>
               <?if (!empty($arItem["PROPERTIES"]["FORUM_MESSAGE_CNT"]["VALUE"])):?>
               <div class="feat-info-comm">
                  <i class="fa fa-comment"></i> <span class="feat-info-text"><?=$arItem["PROPERTIES"]["FORUM_MESSAGE_CNT"]["VALUE"]?></span>
               </div>
               <!--feat-info-comm-->
               <?endif;?>
            </div>
            <!--feat-info-wrap-->
         </div>
         <!--row-widget-img-->
         <div class="row-widget-text left relative" style="height: 70px;">
            <span class="side-list-cat"><?=strip_tags($arItem["DISPLAY_PROPERTIES"]["THEME"]["DISPLAY_VALUE"])?></span>
            <p><?echo $arItem["NAME"]?></p>
         </div>
         <!--row-widget-text-->
      </a>
   </li>
	<?if ($i==3):?></ul><ul class="row-widget-list"><?endif;?>
   <?endforeach;?>
</ul>
