<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arCountry_ = $arCountry = array();
$arCity_ = $arCity = array();

foreach ($arResult as $key=>$arItem) {

    if (!empty($arItem["PROPERTIES"]["COUNTRY"]["VALUE"])) {
        $arCountry[] = $arItem["PROPERTIES"]["COUNTRY"]["VALUE"];
    }
    if (!empty($arItem["PROPERTIES"]["CITY"]["VALUE"])) {
        $arCity[] = $arItem["PROPERTIES"]["CITY"]["VALUE"];
    }

    if (!empty($arItem["PROPERTIES"]["DATE"]["VALUE"])) {

        $date = array();

        if (is_array($arItem["PROPERTIES"]["DATE"]["VALUE"])) {
            $date_ = date("d.m.Y");
            foreach ($arItem["PROPERTIES"]["DATE"]["VALUE"] as $t => $item) {

                if (strtotime($date_) < strtotime($item)) {
                    $date[] = CIBlockFormatProperties::DateFormat("d.m.Y", MakeTimeStamp($item, CSite::GetDateFormat()));
                }
                /*if (count($date) == 4)
                    break;*/

            }
        }

        $arResult[$key]["DATES"] = $date;

    }
}

if(!empty($arCountry)) {

    $res = CIBlockElement::GetList(Array(), Array("IBLOCK_ID" => COUNTRIES_ID_IBLOCK, "ID" =>$arCountry), false, false, Array("IBLOCK_ID", "ID", "NAME"));
    while($ar_fields = $res->GetNext())
    {
        $arCountry_[$ar_fields["ID"]] = $ar_fields["NAME"];
    }

}
if(!empty($arCity)) {

    $res = CIBlockElement::GetList(Array(), Array("IBLOCK_ID" => CITIES_ID_IBLOCK, "ID" =>$arCity), false, false, Array("IBLOCK_ID", "ID", "NAME"));
    while($ar_fields = $res->GetNext())
    {
        $arCity_[$ar_fields["ID"]] = $ar_fields["NAME"];
    }

}

foreach ($arResult as $key=>$arItem) {

    if (!empty($arItem["PROPERTIES"]["COUNTRY"]["VALUE"]) && isset($arCountry_[$arItem["PROPERTIES"]["COUNTRY"]["VALUE"]])) {
        $arResult[$key]["PROPERTIES"]["COUNTRY"]["DISPLAY_VALUE"] = $arCountry_[$arItem["PROPERTIES"]["COUNTRY"]["VALUE"]];
    }
    if (!empty($arItem["PROPERTIES"]["CITY"]["VALUE"]) && isset($arCity_[$arItem["PROPERTIES"]["CITY"]["VALUE"]])) {
        $arResult[$key]["PROPERTIES"]["CITY"]["DISPLAY_VALUE"] = $arCountry_[$arItem["PROPERTIES"]["CITY"]["VALUE"]];
    }

}
