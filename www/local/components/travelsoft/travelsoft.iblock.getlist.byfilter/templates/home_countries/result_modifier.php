<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$currency = array();
$currency_db = CIBlockElement::GetList(Array("SORT"=>"ASC"), Array("IBLOCK_ID"=> 29,"ACTIVE"=>"Y"),false, false, Array("ID", "NAME"));
while($ar_fields = $currency_db->GetNext())
{
    $currency[$ar_fields["ID"]] = $ar_fields["NAME"];
}

if(!empty($currency)) {
    foreach ($arResult as $key => $arItem) {

        if (!empty($arItem["PROPERTIES"]["CURRENCY"]["VALUE"])) {

            $arResult[$key]["PROPERTIES"]["CURRENCY"]["DISPLAY_VALUE"] = $currency[$arItem["PROPERTIES"]["CURRENCY"]["VALUE"]];

        }

    }
}