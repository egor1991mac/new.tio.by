<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
$this->setFrameMode(true);
?>

<?if(!empty($arResult)):?>


    <div class="ts-wrap ts-my-6">
    <?if(!empty($arParams["TEXT_TITLE"])):?>
        <h2 class="ui-title-inner ui-title-inner_mrg-btn_lg">
            <span class="ui-title-inner__inner"><?=htmlspecialchars_decode($arParams["TEXT_TITLE"])?></span>
        </h2>
    <?endif?>

        <div class="owl-carousel owl-theme">
        <?foreach($arResult as $arItem):?>
            <?
            $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
            $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
            $img = getSrc($arItem["PROPERTIES"]["MORE_PHOTO"]["VALUE"], array('width' => 360, 'height' => 220));
            if(empty($img[0])){
                $img = getSrc($arItem["PROPERTIES"]["PHOTO3"]["VALUE"], array('width' => 360, 'height' => 220), NO_PHOTO_PATH_360_220);
            }?>


                <section class="b-post b-post_mod-h clearfix">
                    <div class="entry-media">
                        <a class="js-zoom-images" href="<?=$arItem["DETAIL_PAGE_URL"]?>">
                            <img class="img-responsive" data-aload="<?=$img[0]?>" alt="<?=$arItem["NAME"]?>">
                        </a>
                    </div>
                    <div class="entry-main">
                        <div class="entry-header">
                            <div class="entry-meta">
                                <span class="entry-meta__item">
                                    <a class="entry-meta__link text-primary" href="<?=$arItem["DETAIL_PAGE_URL"]?>">
                                        <strong><?=$arItem["DISPLAY_ACTIVE_FROM"]?></strong>
                                    </a>
                                </span>
                                <?if(in_array(array(1,7),$GLOBALS["USER"]->GetUserGroupArray())):?>
                                    <span class="entry-meta__item"><i class="icon fa fa-heart-o text-second"></i>
                                        <a class="entry-meta__link text-primary" href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=$arResult["SHOW_COUNTER"]?></a>
                                    </span>
                                <?endif?>
                            </div>
                            <h2 class="entry-title"><a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=$arItem["NAME"]?></a></h2>
                        </div>
                    </div>
                </section>


        <?endforeach;?>
        </div>
    </div>

<?endif?>

<script>
    $(document).ready(function(){
        $('.owl-carousel').owlCarousel({
            loop:true,
            margin:10,
            nav:false,
            lazyLoad:true,
            autoplay:true,
            autoplayTimeout:5000,
            autoplayHoverPause:true,
            responsive:{
                0:{
                    items:1
                },
                600:{
                    items:3
                },
            }
        })
    })

</script>