<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
$this->setFrameMode(true);
?>

<?if(!empty($arResult)):?>

<div class="row">
    <section class="section-sm-top wrap-inl-bl">

        <?if(!empty($arParams["TEXT_TITLE"])):?>
            <h2 class="ui-title-inner ui-title-inner_mrg-btn_lg">
                <span class="ui-title-inner__inner"><?=htmlspecialchars_decode($arParams["TEXT_TITLE"])?></span>
            </h2>
        <?endif?>
        <div class="row row-flex">

            <?foreach($arResult as $arItem):?>
                <?
                $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
                $img = getSrc($arItem["PROPERTIES"]["PHOTO"]["VALUE"], array('width' => 360, 'height' => 220), NO_PHOTO_PATH_360_220);
                $comma = false;?>


                <div class="col-sm-6 mrg-bt-30">
                    <div class="b-post b-post_mod-h b-post_bg-white clearfix hover-title ">
                        <div class="entry-media">
                            <a class="js-zoom-images img-hover-effect b-post_img-bg_type-2 " href="<?=$arItem["DETAIL_PAGE_URL"]?>">
                                <img class="img-responsive" data-aload="<?=$img[0]?>" alt="<?=$arItem["NAME"]?>">
                                <div class="b-text__inner">
                                    <?if(!empty($arItem["PROPERTIES"]["MAIN_OFFICE"]["DISPLAY_VALUE"])):?>
                                        <span class="entry-label bg-second"><?=$arItem["PROPERTIES"]["MAIN_OFFICE"]["DISPLAY_VALUE"]?></span>
                                    <?endif?>
                                    <?$country = '';?>
                                    <?if(!empty($arItem["PROPERTIES"]["COUNTRY"]["DISPLAY_VALUE"])):?>
                                        <?$comma = true;?>
                                        <?$country .= '<span class="hov-title__inner">'.$arItem["PROPERTIES"]["COUNTRY"]["DISPLAY_VALUE"].'</span>';?>
                                    <?endif?>
                                    <?if(!empty($arItem["PROPERTIES"]["CITY"]["DISPLAY_VALUE"])):?>
                                        <?$country .= '<span class="hov-title__inner">';?>
                                        <?if($comma):?><?$country .= ': ';?><?endif?>
                                        <?$country .= implode(" / ", $arItem["PROPERTIES"]["CITY"]["DISPLAY_VALUE"]).'</span>';?>
                                    <?endif?>
                                    <?if(!empty($country)):?>
                                        <span class="entry-title hov-title"><?=$country?></span>
                                    <?endif?>
                                </div>
                            </a>
                        </div>
                        <div class="entry-main prom-tour">
                            <div class="entry-header">
                                <div class="entry-meta">
                                    <?if(isset($arItem["DATES"]) && !empty($arItem["DATES"])):?>
                                        <span class="entry-meta__item">
                                        <i class="icon fa-calendar text-second"></i>
                                        <span class="entry-meta__link text-primary">
                                            <strong><?=implode(', ', $arItem["DATES"])?></strong>
                                        </span>
                                    </span>
                                    <?endif?>
                                </div>
                                <div class="entry-title"><a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=$arItem["NAME"]?></a></div>
                            </div>
                            <div class="entry-content">
                                <?if(!empty($arItem["PROPERTIES"]["TEXT_PREVIEW"]["VALUE"]["TEXT"])):?>
                                    <p>
                                        <?=substr2($arItem["PROPERTIES"]["TEXT_PREVIEW"]["~VALUE"]["TEXT"], 94)?>
                                    </p>
                                <?elseif(!empty($arItem["PROPERTIES"]["TEXT"]["VALUE"]["TEXT"])):?>
                                    <p>
                                        <?=substr2($arItem["PROPERTIES"]["TEXT"]["~VALUE"]["TEXT"], 94)?>
                                    </p>
                                <?endif;?>
                                <?$cont = '';$icon = '';?>
                                <?if(!empty($arItem["PROPERTIES"]["CITY_TRIP"]["VALUE"])):?>
                                    <?$icon = '<i class="b-contacts__icon fa fa-map-marker"></i>';?>
                                    <?$cont .= GetMessage('DEPARTURE') . $arItem["PROPERTIES"]["CITY_TRIP"]["DISPLAY_VALUE"]?>
                                <?endif?>
                                <?if(!empty($arItem["PROPERTIES"]["NIGHTS"]["VALUE"])):?>
                                    <?if(!empty($cont)):?><?$cont .= ', ';?><?endif?>
                                    <?$cont .= GetMessage("FROM_NIGHTS") . num2word($arItem["PROPERTIES"]["NIGHTS"]["VALUE"], array(GetMessage('NIGHT1'),GetMessage('NIGHT2'),GetMessage('NIGHT_ALL')))?>
                                <?endif?>
                                <?if(!empty($cont)):?>
                                    <span class="entry-meta__item">
                                        <?=$icon?>
                                        <span class="entry-meta__link">
                                            <strong><?=$cont?></strong>
                                        </span>
                                    </span>
                                <?endif?>
                            </div>
                        </div>
                    </div>
                </div>

            <?endforeach;?>

        </div>

    </section>
</div>

<?endif?>

