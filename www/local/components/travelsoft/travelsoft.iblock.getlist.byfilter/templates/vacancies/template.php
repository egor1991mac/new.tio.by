<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
$this->setFrameMode(true);
?>

<?if(!empty($arResult)):?>

    <div class="widget section-sidebar left_block">
        <?if(!empty($arParams["TEXT_TITLE"])):?>
            <div class="widget-title ui-title-inner ui-title-inner_lg">
                <span class="ui-title-inner__inner"><?=htmlspecialchars_decode($arParams["TEXT_TITLE"])?></span>
            </div>
        <?endif?>
        <div class="widget-content">
            <div class="posts-group-4">
                <?foreach($arResult as $arItem):?>
                    <?
                    $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                    $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
                    ?>

                    <section class="b-post vacancies_block clearfix" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
                        <div class="entry-main">
                            <div class="entry-header">
                                <div class="entry-title"><a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=$arItem["NAME"]?></a></div>
                                <?if(isset($arItem["PROPERTIES"]["COMPANY"]["DISPLAY_VALUE"])):?>
                                    <div class="entry-meta">
                                        <span class="entry-meta__item">
                                            <a class="entry-meta__link" href="<?=$arItem["DETAIL_PAGE_URL"]?>">
                                                <strong><?=$arItem["PROPERTIES"]["COMPANY"]["DISPLAY_VALUE"]?></strong>
                                            </a>
                                        </span>
                                    </div>
                                <?endif?>
                            </div>
                        </div>
                    </section>

                <?endforeach;?>
            </div>
        </div>
    </div>

<?endif?>

