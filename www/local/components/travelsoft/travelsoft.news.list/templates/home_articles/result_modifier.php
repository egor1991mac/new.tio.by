<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

foreach ($arResult["ITEMS"] as $key=>$arItem){

    if(!empty($arItem["DISPLAY_PROPERTIES"]["AUTHOR"]["VALUE"])){

        $rsUser = CUser::GetByID($arItem["DISPLAY_PROPERTIES"]["AUTHOR"]["VALUE"]);
        $arUser = $rsUser->Fetch();
        $arResult["ITEMS"][$key]["DISPLAY_PROPERTIES"]["AUTHOR"]["DESC"] = $arUser["NAME"]." ".$arUser["LAST_NAME"];

    }

}