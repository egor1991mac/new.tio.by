<?
$MESS["CT_BNL_ELEMENT_DELETE_CONFIRM"] = "Будет удалена вся информация, связанная с этой записью. Продолжить?";
$MESS["STOCK_MORE_INFO"] = "Подробнее";
$MESS["STOCK_ORDER"] = "Заказать";
$MESS["STOCK_PRICE_FOR"] = "от";
$MESS["STOCK_ADULTS"] = "взрослых";
$MESS["STOCK_CHILDREN"] = "детей";

$MESS["NIGHT_1"] = "ночь";
$MESS["NIGHT_2"] = "ночи";
$MESS["NIGHT_ALL"] = "ночей";

$MESS["ADULTS_1"] = "взрослый";
$MESS["ADULTS_2"] = "взрослых";
$MESS["ADULTS_ALL"] = "взрослых";

$MESS["CHILDREN_1"] = "ребенок";
$MESS["CHILDREN_2"] = "ребенка";
$MESS["CHILDREN_ALL"] = "детей";
?>