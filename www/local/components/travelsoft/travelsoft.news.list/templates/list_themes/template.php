<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<?$cnt = count($arResult["ITEMS"]);
$del = $cnt / 3;
$ost = $cnt % 3;
if($ost != 0) $del++;
?>

<ul class="list list-mark-4 ts-col-24 ts-col-md-8">
    <?$i = 1;?>
    <? foreach ($arResult["ITEMS"] as $arItem): ?>
        <? if (!empty($arItem['COUNT_NEWS'])): ?>
            <?
            $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
            $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
            ?>
            <li class="widget-list__item" id="<?= $this->GetEditAreaId($arItem['ID']); ?>">
                <a class="widget-list__link" href="<?= $arItem['DETAIL_PAGE_URL'] ?>">
                    <?= $arItem['NAME'] ?>
                    <span class="badge"><?= $arItem['COUNT_NEWS']; ?></span>
                </a>
            </li>

            <?if($i % $del == 0 && $i != $cnt):?>
                </ul>
                <ul class="list list-mark-4 col-md-4">
            <?endif;?>

            <?$i++;?>
        <? endif; ?>
    <? endforeach; ?>
</ul>

<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
	<?=$arResult["NAV_STRING"]?>
<?endif;?>

