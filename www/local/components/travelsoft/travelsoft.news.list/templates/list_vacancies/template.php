<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<?if(!empty($arResult["ITEMS"])):?>

    <?if(!empty($arParams["TEXT_TITLE"])):?>
        <h4 class="widget-title ui-title-inner ui-title-inner_lg">
            <span class="ui-title-inner__inner"><?=htmlspecialchars_decode($arParams["TEXT_TITLE"])?></span>
        </h4>
    <?endif?>
        <div class="posts-group">
            <?foreach($arResult["ITEMS"] as $arItem):?>
                <?
                $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
                $img = getSrc($arItem["DISPLAY_PROPERTIES"]["PICTURES"]["VALUE"], array('width' => 260, 'height' => 160), NO_PHOTO_PATH_260_160) 
                ?>
                  <section class="b-post b-post_bg-blue ts-mb-6 " id="<?=$this->GetEditAreaId($arItem['ID']);?>">
                        <div class="ts-row">
                          <div class="entry-media ts-col-24 ts-col-sm-12">
                              <a class="ts-width-100" href="<?=$arItem["DETAIL_PAGE_URL"]?>">
                                  <img class="ts-width-100" src="<?=$img[0]?>" alt="<?=$arItem["NAME"]?>"/>
                              </a>
                          </div>
                         <div class="entry-main ts-p-4 ts-px-sm-2 ts-col-24 ts-col-sm-12">
                          <div class="entry-header">
                            <h4 class="entry-title ts-mt-0 ts-pt-0"><a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=$arItem["NAME"]?></a></h4>
                              <div class="entry-meta"><?if(!empty($arItem["DISPLAY_PROPERTIES"]["COMPANY"]["DISPLAY_VALUE"])):?><span class="entry-meta__item"><?=$arItem["DISPLAY_PROPERTIES"]["COMPANY"]["DISPLAY_VALUE"]?></span><?endif;?><?if(!empty($arItem["DISPLAY_PROPERTIES"]["TYPE_PUBLIC"]["VALUE"])):?><span class="entry-meta__item"><?=$arItem["DISPLAY_PROPERTIES"]["TYPE_PUBLIC"]["VALUE"]?></span><?endif;?><?if(!empty($arItem["DISPLAY_ACTIVE_FROM"])):?><span class="entry-meta__item"><strong><?=$arItem["DISPLAY_ACTIVE_FROM"]?></strong></span><?endif?><?if(!empty($arItem["SHOW_COUNTER"])):?><span class="entry-meta__item"><i class="icon fa fa-eye text-second"></i><?=$arItem["SHOW_COUNTER"]?></span><?endif;?>
                            </div>
                          </div>
                          <div class="entry-content">
                                <?if(!empty($arItem["PREVIEW_TEXT"])):?>
                                    <p><?=substr2($arItem["~PREVIEW_TEXT"], 180)?></p>
                                <?elseif(!empty($arItem["DETAIL_TEXT"])):?>
                                    <p><?=substr2($arItem["~DETAIL_TEXT"], 180)?></p>
                                <?endif?>
                          </div>
					    <div class="entry-footer"><a class="btn-bd-primary" href="<?=$arItem["DETAIL_PAGE_URL"]?>">подробнее</a></div>
                         </div>
                    </div>
                  </section>
            <?endforeach;?>
        </div>
<?endif?>

