<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();


$arResult["GROUP"] = array();
foreach ($arResult["ITEMS"] as $key=>$arItem){

    if($arItem["IBLOCK_SECTION_ID"]){

        if(!isset($arResult["GROUP"][$arItem["IBLOCK_SECTION_ID"]])){

            $res = CIBlockSection::GetByID($arItem["IBLOCK_SECTION_ID"]);
            $ar_res = $res->GetNext();

            $arResult["GROUP"][$arItem["IBLOCK_SECTION_ID"]] = array(
                "ID" => $arItem["IBLOCK_SECTION_ID"],
                "NAME" => $ar_res["NAME"],
                "ITEMS" => array()
            );

        }

        $arResult["GROUP"][$arItem["IBLOCK_SECTION_ID"]]["ITEMS"][$arItem["ID"]] = array(
            "ID" => $arItem["ID"],
            "NAME" => $arItem["NAME"],
            "DETAIL_PAGE_URL" => $arItem["DETAIL_PAGE_URL"],
            "FLAG" => getSrc($arItem["DISPLAY_PROPERTIES"]["FLAG"]["VALUE"], array("width"=>24,"height"=>24)),
            "PRICE" => $arItem["DISPLAY_PROPERTIES"]["PRICE"]["VALUE"],
            "CURRENCY" => strip_tags($arItem["DISPLAY_PROPERTIES"]["CURRENCY"]["DISPLAY_VALUE"])
        );

    }

}

foreach ($arResult["GROUP"] as $key=>$group){

    $arResult["GROUP"][$key]["ITEMS"] = customMultiSort($group["ITEMS"],"NAME",true);

}

$arResult["GROUP"] = customMultiSort($arResult["GROUP"],"NAME",true);