<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
if (strlen($arResult["PAGE_URL"]) > 0)
{
	?>
    <span class="entry-footer__title"><?=GetMessage('SHARE')?></span>
        <?
		if (is_array($arResult["BOOKMARKS"]) && count($arResult["BOOKMARKS"]) > 0)
		{
			?>
            <ul class="social-net list-inline">
                <?
                foreach($arResult["BOOKMARKS"] as $name => $arBookmark)
                {
                    ?><li class="social-net__item"><?=$arBookmark["ICON"]?></li><?
                }
                ?>
            </ul>
            <?
		}
		?>
        <?
}
else
{
	?><?=GetMessage("SHARE_ERROR_EMPTY_SERVER")?><?
}
?>
