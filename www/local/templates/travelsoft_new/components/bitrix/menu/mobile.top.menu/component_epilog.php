<?
global $APPLICATION;
$request = \Bitrix\Main\Context::getCurrent()->getRequest();

if ($request->get("__ajax_component") === "component_menu") {
    $APPLICATION->RestartBuffer();
    header('Content-Type: application/json');
    echo \json_encode($arResult);
    die();
}