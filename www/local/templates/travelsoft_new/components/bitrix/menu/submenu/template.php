<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<? if (!empty($arResult)): ?>
    <section class="widget section-sidebar ts-m-0 ts-width-100 ts-mb-6">
        <div class="widget-content ts-m-0">
            <ul class="widget-list list list-mark-4 ts-m-0">

                <? foreach ($arResult as $arItem): ?>
                    <? if ($arParams["MAX_LEVEL"] == 1 && $arItem["DEPTH_LEVEL"] > 1) continue; ?>

                    <li class="widget-list__item <? if ($arItem["SELECTED"]): ?>active<? endif; ?>">
                        <a class="widget-list__link" href="<?= $arItem["LINK"] ?>"><?= $arItem["TEXT"] ?></a>
                    </li>

                <? endforeach ?>

            </ul>
        </div>
    </section>
<? endif ?>