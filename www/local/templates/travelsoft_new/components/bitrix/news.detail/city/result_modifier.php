<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

if(!empty($arResult["DISPLAY_PROPERTIES"]["PHOTO"]["VALUE"]) && count($arResult["DISPLAY_PROPERTIES"]["PHOTO"]["VALUE"]) > 1){

    $arResult["SLIDER1"] =  getSrc($arResult["DISPLAY_PROPERTIES"]["PHOTO"]["VALUE"], array('width' => 740, 'height' => 533));

}

$arResult["ROUTER"] = array(
    "TOURS" => false,
    "HOTELS" => false
);


/*if($arResult["DISPLAY_PROPERTIES"]["SHOW_SEARCH_FORM_TOURS"]["VALUE"] == "Y" || !empty($arResult["DISPLAY_PROPERTIES"]["TOURS_TEXT"]["VALUE"]["TEXT"])) {
    $arResult["ROUTER"]["TOURS"] = true;
}*/

if(!empty($arResult["PROPERTIES"]["MAP"]["VALUE"])) {
    $LATLNG = explode(",", $arResult["PROPERTIES"]["MAP"]["VALUE"]);
    $arResult['MAP_SCALE'] = !empty($arResult["PROPERTIES"]["MAP_ZOOM"]["VALUE"]) ? $arResult["PROPERTIES"]["MAP_ZOOM"]["VALUE"] : 8;
    $arResult['ROUTE_INFO'][] = array(
        "lat" => $LATLNG[0],
        "lng" => $LATLNG[1],
        "title" => $arResult['NAME'],
        "infoWindow" => "<div style='color:red'><b>" . $arResult['NAME'] . "</b></div>"
    );
}