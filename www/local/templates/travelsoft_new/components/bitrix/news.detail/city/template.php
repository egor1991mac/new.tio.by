<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

//$this->addExternalCss(SITE_TEMPLATE_PATH."/assets/plugins/slider-pro/slider-pro.css");
//$this->addExternalJS(SITE_TEMPLATE_PATH."/assets/plugins/slider-pro/jquery.sliderPro.min.owl");

$htmlMapID = "route-map";
$scroll = array();
$img = getSrc($arResult["DISPLAY_PROPERTIES"]["PHOTO"]["VALUE"], array('width' => 1140, 'height' => 550));
?>

<section class="b-post b-post_img-bg b-post_img-bg_type-8 clearfix">
    <?if(!empty($img[0])):?>
        <div class="entry-media">
            <img class="img-responsive" src="<?=$img[0]?>" alt="<?=$arResult["NAME"]?>">
        </div>
    <?endif?>
    <div class="entry-main<?if(empty($img[0])):?> section-block-nophoto<?endif?>">
        <div class="entry-header">
            <div class="entry-meta">
                <?if(in_array(array(1,7),$GLOBALS["USER"]->GetUserGroupArray())):?>
                    <span class="entry-meta__item"><i class="icon fa fa-heart-o text-second"></i>
                        <a class="entry-meta__link"><?=$arResult["SHOW_COUNTER"]?></a>
                    </span>
                <?endif?>
            </div>
            <h1 class="entry-title hov-title">
                <a class="hov-title__inner"><?=$arResult["NAME"]?></a>
            </h1>
        </div>
        <?if(!empty($arResult["PREVIEW_TEXT"])):?>
            <div class="entry-content">
                <p><?=$arResult["~PREVIEW_TEXT"]?></p>
            </div>
        <?endif?>
    </div>
</section>
<div class="col-md-8">

    <ul class="nav nav-tabs nav-tabs_top-header">
        <li class="active"><a href="#desc" data-toggle="tab"><?=GetMessage('DESCRIPTION')?></a></li>
        <?if($arResult["ROUTER"]["TOURS"]):?>
            <li><a href="<?=$arParams["DETAIL_PAGE"]?>tours/"><?=GetMessage('TOURS')?></a></li>
        <?endif?>
        <?if($arResult["ROUTER"]["HOTELS"]):?>
            <li><a href="<?=$arParams["DETAIL_PAGE"]?>hotels/"><?=GetMessage('HOTELS')?></a></li>
        <?endif?>
        <?if(!empty($arResult["ROUTE_INFO"])):?>
            <li><a class="map_tab" rel="nofollow" href="#map" data-toggle="tab"><?=GetMessage('MAP')?></a></li>
        <?endif?>
    </ul>

    <div class="tab-content">
        <div class="tab-pane fade in active" id="desc">

            <div class="l-main-content l-main-content_mrg-right_minus">
                <article class="b-post b-post-full clearfix">
                    <div class="entry-main">

                        <div class="entry-content">

                            <?if(!empty($arResult["DISPLAY_PROPERTIES"]["COUNTRY"]["VALUE"]) || !empty($arResult["DISPLAY_PROPERTIES"]["AIR_TEMPERATURE"]["VALUE"]) || !empty($arResult["DISPLAY_PROPERTIES"]["WATER_TEMPERATURE"]["VALUE"])):?>
                                <div class="table-container">
                                    <table class="table table_secondary table-type-2 table-striped typography-last-elem">
                                        <tbody>
                                        <?if(!empty($arResult["DISPLAY_PROPERTIES"]["COUNTRY"]["VALUE"])):?>
                                            <tr>
                                                <td class="td_name"><?=$arResult["DISPLAY_PROPERTIES"]["COUNTRY"]["NAME"]?></td>
                                                <td><?=strip_tags($arResult["DISPLAY_PROPERTIES"]["COUNTRY"]["DISPLAY_VALUE"])?></td>
                                            </tr>
                                        <?endif?>
                                        <?if(!empty($arResult["DISPLAY_PROPERTIES"]["AIR_TEMPERATURE"]["VALUE"])):?>
                                            <tr>
                                                <td class="td_name"><?=$arResult["DISPLAY_PROPERTIES"]["AIR_TEMPERATURE"]["NAME"]?></td>
                                                <td><?=$arResult["DISPLAY_PROPERTIES"]["AIR_TEMPERATURE"]["VALUE"]?></td>
                                            </tr>
                                        <?endif?>
                                        <?if(!empty($arResult["DISPLAY_PROPERTIES"]["WATER_TEMPERATURE"]["VALUE"])):?>
                                            <tr>
                                                <td class="td_name"><?=$arResult["DISPLAY_PROPERTIES"]["WATER_TEMPERATURE"]["NAME"]?></td>
                                                <td><?=$arResult["DISPLAY_PROPERTIES"]["WATER_TEMPERATURE"]["VALUE"]?></td>
                                            </tr>
                                        <?endif?>
                                        </tbody>
                                    </table>
                                </div>
                            <?endif?>

                            <div class="block_desc" id="block_desc">
                                <?$scroll[] = array("id"=>'block_desc',"name"=>GetMessage('DESC'));?>
                                <p><?=$arResult["~DETAIL_TEXT"]?></p>
                                <br>
                            </div>
                            <?if(isset($arResult["SLIDER1"]) && !empty($arResult["SLIDER1"])):?>
                                <div class="block_photo" id="block_photo">
                                    <?$scroll[] = array("id"=>'block_photo',"name"=>GetMessage('PHOTO'));?>
                                    <h3><?=GetMessage('PHOTO')?></h3>
                                    <div class="main-slider slider-pro" id="main-slider1" data-slider-width="100%" data-slider-height="550px" data-slider-arrows="false" data-slider-buttons="false" style="margin: 0">
                                        <div class="sp-slides">
                                            <?foreach ($arResult["SLIDER1"] as $k=>$slide):?>
                                                <?$slide_desc = $arResult["DISPLAY_PROPERTIES"]["MORE_PHOTO"]["DESCRIPTION"][$k];?>
                                                <div class="sp-slide">
                                                    <img class="sp-image" src="<?=$slide?>" alt="<?=!empty($slide_desc) ? $slide_desc : 'slide-'.$k+1?>"/>
                                                    <?if(!empty($slide_desc)):?>
                                                        <div class="main-slider__info sp-layer" data-width="100%" data-show-transition="left" data-hide-transition="left" data-show-duration="2000" data-show-delay="1200" data-hide-delay="400">
                                                            <div class="container">
                                                                <div class="row">
                                                                    <div class="col-md-10">
                                                                        <section class="b-post b-post_slider clearfix">
                                                                            <div class="entry-main">
                                                                                <div class="entry-header">
                                                                                    <h2 class="entry-title hov-title"><a class="hov-title__inner"><?=$slide_desc?></a></h2>
                                                                                </div>
                                                                            </div>
                                                                        </section>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    <?endif?>
                                                </div>
                                            <?endforeach;?>
                                        </div>
                                    </div>
                                </div>
                            <?endif?>

                            <?if(!empty($arResult["DISPLAY_PROPERTIES"]["THINGS_TO_DO"]["VALUE"]["TEXT"])):?>
                                <div class="block_info" id="block_info">
                                    <?$scroll[] = array("id"=>'block_info',"name"=>$arResult["DISPLAY_PROPERTIES"]["THINGS_TO_DO"]["NAME"]);?>
                                    <h3><?=$arResult["DISPLAY_PROPERTIES"]["THINGS_TO_DO"]["NAME"]?></h3>
                                    <p><?=$arResult["DISPLAY_PROPERTIES"]["THINGS_TO_DO"]["DISPLAY_VALUE"]?></p>
                                </div>
                            <?endif?>
                            <?if(!empty($arResult["DISPLAY_PROPERTIES"]["WEATHER"]["VALUE"]["TEXT"])):?>
                                <div class="block_klimat" id="block_klimat">
                                    <?$scroll[] = array("id"=>'block_klimat',"name"=>$arResult["DISPLAY_PROPERTIES"]["WEATHER"]["NAME"]);?>
                                    <h3><?=$arResult["DISPLAY_PROPERTIES"]["WEATHER"]["NAME"]?></h3>
                                    <p><?=$arResult["DISPLAY_PROPERTIES"]["WEATHER"]["DISPLAY_VALUE"]?></p>
                                </div>
                            <?endif?>
                            <?if(!empty($arResult["DISPLAY_PROPERTIES"]["BEACH"]["VALUE"]["TEXT"])):?>
                                <div class="block_beaches" id="block_beaches">
                                    <?$scroll[] = array("id"=>'block_beaches',"name"=>$arResult["DISPLAY_PROPERTIES"]["BEACH"]["NAME"]);?>
                                    <h3><?=$arResult["DISPLAY_PROPERTIES"]["BEACH"]["NAME"]?></h3>
                                    <p><?=$arResult["DISPLAY_PROPERTIES"]["BEACH"]["DISPLAY_VALUE"]?></p>
                                </div>
                            <?endif?>
                            <?if(!empty($arResult["DISPLAY_PROPERTIES"]["FEATURES"]["VALUE"]["TEXT"])):?>
                                <div class="block_features" id="block_features">
                                    <?$scroll[] = array("id"=>'block_features',"name"=>$arResult["DISPLAY_PROPERTIES"]["FEATURES"]["NAME"]);?>
                                    <h3><?=$arResult["DISPLAY_PROPERTIES"]["FEATURES"]["NAME"]?></h3>
                                    <p><?=$arResult["DISPLAY_PROPERTIES"]["FEATURES"]["DISPLAY_VALUE"]?></p>
                                </div>
                            <?endif?>
                            <?if(!empty($arResult["DISPLAY_PROPERTIES"]["INFORMATION"]["VALUE"]["TEXT"])):?>
                                <div class="block_inf" id="block_inf">
                                    <?$scroll[] = array("id"=>'block_inf',"name"=>$arResult["DISPLAY_PROPERTIES"]["INFORMATION"]["NAME"]);?>
                                    <h3><?=$arResult["DISPLAY_PROPERTIES"]["INFORMATION"]["NAME"]?></h3>
                                    <p><?=$arResult["DISPLAY_PROPERTIES"]["INFORMATION"]["DISPLAY_VALUE"]?></p>
                                </div>
                            <?endif?>

                        </div>


                        <div class="entry-footer">
                            <?if(!empty($arResult["DISPLAY_PROPERTIES"]["FEATURES_LIST"]["VALUE"])):?>
                                <div class="post-tags"><span class="entry-footer__title"><?=GetMessage('THEME')?></span>
                                    <ul class="list-tags list-tags_grey list-unstyled">
                                        <?if(isset($arResult["DISPLAY_PROPERTIES"]["FEATURES_LIST"]["LINK_ELEMENT_VALUE"]) && !empty($arResult["DISPLAY_PROPERTIES"]["FEATURES_LIST"]["LINK_ELEMENT_VALUE"])):?>
                                            <?foreach ($arResult["DISPLAY_PROPERTIES"]["FEATURES_LIST"]["LINK_ELEMENT_VALUE"] as $theme):?>
                                                <li class="list-tags__item"><a class="list-tags__link btn btn-default" href="<?=$theme["DETAIL_PAGE_URL"]?>"><?=$theme["NAME"]?></a></li>
                                            <?endforeach;?>
                                        <?else:?>
                                            <?if(is_array($arResult["DISPLAY_PROPERTIES"]["FEATURES_LIST"]["DISPLAY_VALUE"])):?>
                                                <?foreach ($arResult["DISPLAY_PROPERTIES"]["FEATURES_LIST"]["DISPLAY_VALUE"] as $theme):?>
                                                    <li class="list-tags__item"><?=$theme?></li>
                                                <?endforeach;?>
                                                <!--<li class="list-tags__item"><?/*=implode2($arResult["DISPLAY_PROPERTIES"]["FEATURES_LIST"]["DISPLAY_VALUE"], '</li><li class="list-tags__item">')*/?></li>-->
                                            <?else:?>
                                                <li class="list-tags__item"><?=$arResult["DISPLAY_PROPERTIES"]["FEATURES_LIST"]["DISPLAY_VALUE"]?></li>
                                            <?endif?>
                                        <?endif?>
                                    </ul>
                                </div>
                            <?endif?>
                            <div class="entry-footer__social">
                                <?$APPLICATION->IncludeComponent(
                                    "bitrix:main.share",
                                    "main",
                                    Array(
                                        "HANDLERS" => array("facebook","twitter","vk"),
                                        "HIDE" => "N",
                                        "PAGE_TITLE" => $APPLICATION->GetTitle(),
                                        "PAGE_URL" => $_SERVER["REQUEST_URI"],
                                        "SHORTEN_URL_KEY" => "",
                                        "SHORTEN_URL_LOGIN" => ""
                                    )
                                );?>
                            </div>
                        </div>
                    </div>
                </article>
            </div>

        </div>
        <?if(!empty($arResult["ROUTE_INFO"])):?>
            <div class="tab-pane fade" id="map">
                <div class="l-main-content l-main-content_mrg-right_minus">
                    <article class="b-post b-post-full clearfix">
                        <div class="entry-main">

                            <div class="entry-content">

                                <?
                                $this->addExternalJs("https://maps.googleapis.com/maps/api/owl?key=AIzaSyBaEtiggot7kJNVdbYyTr9imXxRipJ7jrM");
                                $this->addExternalJS(SITE_TEMPLATE_PATH."/assets/owl/jquery-custom-google-map-lib.owl");
                                ?>
                                <div style="width: 100%; height: 432px" id="<?= $htmlMapID ?>"></div>

                            </div>

                        </div>
                    </article>
                </div>
            </div>
        <?endif?>

    </div>

</div>
<div class="col-md-4">
    <aside class="l-sidebar l-sidebar_mrg-left l-sidebar_first-section" id="sidebar">
        <?if(!empty($scroll)):?>
            <!--<div class="theiaStickySidebar" style="top:120px !important;">-->
                <div class="widget-content">
                    <ul class="widget-list widget-list_white list list-mark-4">
                        <?foreach($scroll as $s):?>
                            <?if(!empty($s)):?>
                                <li class="widget-list__item">
                                    <a class="widget-list__link anchor_" href="#<?= $s["id"]?>"><?= $s["name"]?></a>
                                </li>
                            <?endif?>
                        <?endforeach?>
                    </ul>
                </div>
           <!-- </div>-->
        <?endif?>
    </aside>
</div>

<script>
    var $block = $("#sidebar");
    var fixedClass = "fixed";
    var active_block = 0;

    var height_content_block = $('.tab-pane.active').height() + 540;

    $('.nav-tabs li a').on("click", function () {

        active_block = $(this).attr('href');
        if(active_block.indexOf('#') >= 0){
            active_block = active_block.replace("#",'',active_block);
            height_content_block = $('#' + active_block).height() + 500;
        }

    });

    $(window).scroll(function(){
        if($(window).scrollTop()>954 && $(window).scrollTop()<height_content_block){
            $block.addClass(fixedClass)
        }else{
            $block.removeClass(fixedClass)
        }
    });



    $('.map_tab').one('click', function () {
        (function (gm) {
            // init map and draw route
            gm.createGoogleMap("<?= $htmlMapID ?>", {center: gm.LatLng(<?= $arResult['ROUTE_INFO'][0]["lat"] ?>, <?= $arResult['ROUTE_INFO'][0]["lng"] ?>), zoom: <?= $arResult['MAP_SCALE'] ?>})
        })(window.GoogleMapFunctionsContainer)
    });

    $( document ).ready(function() {
        $(".entry-content img").each(function () {
                $(this).addClass('img-responsive');
            }
        );
        $(".entry-footer .list-tags li").each(function () {
                if($(this).find('a').hasClass('list-tags__link') === false){
                    $(this).find('a').addClass('list-tags__link btn btn-default');
                }
            }
        );

        $('a.anchor_').on("click", function () {

            if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {

                var target = $(this.hash);
                target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');

                if($('.tab-pane.active').attr('id') != 'desc'){

                    $('.nav-tabs li a[href="#desc"]').trigger('click');

                    setTimeout(function () {
                        $('html,body').stop().animate({
                            scrollTop: (target.offset().top - 135) // 70px offset for navbar menu
                        }, 1000);
                    }, 1000);


                } else {
                    $('html,body').stop().animate({
                        scrollTop: (target.offset().top - 135) // 70px offset for navbar menu
                    }, 1000);
                }
                return false;

            }
        });

    });
</script>