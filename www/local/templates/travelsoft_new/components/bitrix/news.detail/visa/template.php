<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);;
?>

    <div class="l-main-content l-main-content_mrg-right_minus">
        <article class="b-post b-post-full clearfix">
            <div class="entry-main">

                <div class="entry-content">

                    <?if(!empty($arResult["DISPLAY_PROPERTIES"]["COUNTRY"]["VALUE"])):?>
                        <div class="table-container">
                            <table class="table table_secondary table-type-2 table-striped typography-last-elem">
                                <tbody>
                                    <tr>
                                        <td class="td_name"><?=$arResult["DISPLAY_PROPERTIES"]["COUNTRY"]["NAME"]?></td>
                                        <td><?=strip_tags($arResult["DISPLAY_PROPERTIES"]["COUNTRY"]["DISPLAY_VALUE"])?></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    <?endif?>

                    <?if(!empty($arResult["DISPLAY_PROPERTIES"]["TEXT"]["VALUE"]["TEXT"])):?>
                        <p><?=$arResult["DISPLAY_PROPERTIES"]["TEXT"]["DISPLAY_VALUE"]?></p>
                        <br>
                    <?endif?>

                    <?if(isset($arResult["SLIDER1"]) && !empty($arResult["SLIDER1"])):?>
                        <div class="block_photo" id="block_photo">
                            <?$scroll[] = array("id"=>'block_photo',"name"=>GetMessage('PHOTO'));?>
                            <h3><?=GetMessage('PHOTO')?></h3>
                            <div class="main-slider slider-pro" id="main-slider1" data-slider-width="78%" data-slider-height="550px" data-slider-arrows="false" data-slider-buttons="false" style="margin: 0">
                                <div class="sp-slides">
                                    <?foreach ($arResult["SLIDER1"] as $k=>$slide):?>
                                        <?$slide_desc = $arResult["DISPLAY_PROPERTIES"]["MORE_PHOTO"]["DESCRIPTION"][$k];?>
                                        <div class="sp-slide">
                                            <img class="sp-image" src="<?=$slide?>" alt="<?=!empty($slide_desc) ? $slide_desc : 'slide-'.$k+1?>"/>
                                            <?if(!empty($slide_desc)):?>
                                                <div class="main-slider__info sp-layer" data-width="100%" data-show-transition="left" data-hide-transition="left" data-show-duration="2000" data-show-delay="1200" data-hide-delay="400">
                                                    <div class="container">
                                                        <div class="row">
                                                            <div class="col-md-10">
                                                                <section class="b-post b-post_slider clearfix">
                                                                    <div class="entry-main">
                                                                        <div class="entry-header">
                                                                            <h2 class="entry-title hov-title"><a class="hov-title__inner"><?=$slide_desc?></a></h2>
                                                                        </div>
                                                                    </div>
                                                                </section>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?endif?>
                                        </div>
                                    <?endforeach;?>
                                </div>
                            </div>
                        </div>
                    <?endif?>

                </div>

            </div>
        </article>
    </div>


<script>

    $( document ).ready(function() {
        $(".entry-content img").each(function () {
                $(this).addClass('img-responsive');
            }
        );
        $(".entry-footer .list-tags li").each(function () {
                if($(this).find('a').hasClass('list-tags__link') === false){
                    $(this).find('a').addClass('list-tags__link btn btn-default');
                }
            }
        );

    });
</script>