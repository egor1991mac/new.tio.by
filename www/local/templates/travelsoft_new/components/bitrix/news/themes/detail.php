<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="ts-col-24 ts-mt-3">
<div class="ts-row">

<div class="ts-col-24 ts-col-md-8">
    <aside class="l-sidebar <?/*l-sidebar_mrg-left*/?> l-sidebar_first-section ts-width-100">
        <?$GLOBALS["arThemes"] = array("PROPERTY_MAIN"=>1);
        $APPLICATION->IncludeComponent(
            "travelsoft:travelsoft.news.list",
            "themes_aside",
            Array(
                "ACTIVE_DATE_FORMAT" => "d.m.Y",
                "ADD_SECTIONS_CHAIN" => "N",
                "AFP_1" => "",
                "AFP_ID" => "",
                "AJAX_MODE" => "N",
                "AJAX_OPTION_ADDITIONAL" => "",
                "AJAX_OPTION_HISTORY" => "N",
                "AJAX_OPTION_JUMP" => "N",
                "AJAX_OPTION_STYLE" => "Y",
                "CACHE_FILTER" => "N",
                "CACHE_GROUPS" => "Y",
                "CACHE_TIME" => "36000000",
                "CACHE_TYPE" => "A",
                "CHECK_DATES" => "Y",
                "COMPOSITE_FRAME_MODE" => "A",
                "COMPOSITE_FRAME_TYPE" => "AUTO",
                "DETAIL_URL" => "",
                "DISPLAY_BOTTOM_PAGER" => "N",
                "DISPLAY_DATE" => "N",
                "DISPLAY_NAME" => "N",
                "DISPLAY_PICTURE" => "N",
                "DISPLAY_PREVIEW_TEXT" => "N",
                "DISPLAY_TOP_PAGER" => "N",
                "FIELD_CODE" => array(0=>"",1=>"",),
                "FILTER_NAME" => "arThemes",
                "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                "IBLOCK_ID" => "1",
                "IBLOCK_TYPE" => "news",
                "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                "INCLUDE_SUBSECTIONS" => "N",
                "MESSAGE_404" => "",
                "NEWS_COUNT" => "10",
                "PAGER_BASE_LINK_ENABLE" => "N",
                "PAGER_DESC_NUMBERING" => "N",
                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                "PAGER_SHOW_ALL" => "N",
                "PAGER_SHOW_ALWAYS" => "N",
                "PAGER_TEMPLATE" => ".default",
                "PAGER_TITLE" => "Новости",
                "PARENT_SECTION" => "",
                "PARENT_SECTION_CODE" => "",
                "PREVIEW_TRUNCATE_LEN" => "",
                "PROPERTY_CODE" => array(0=>"",1=>"",),
                "SET_BROWSER_TITLE" => "N",
                "SET_LAST_MODIFIED" => "N",
                "SET_META_DESCRIPTION" => "N",
                "SET_META_KEYWORDS" => "N",
                "SET_STATUS_404" => "N",
                "SET_TITLE" => "N",
                "SHOW_404" => "N",
                "SORT_BY1" => "ACTIVE_FROM",
                "SORT_BY2" => "SORT",
                "SORT_ORDER1" => "DESC",
                "SORT_ORDER2" => "ASC"
            )
        );?>
    </aside>
    <?
    $APPLICATION->IncludeFile($APPLICATION->GetCurDir()."sect_left_inc.php", Array(), Array(
        "MODE"=>"html",
        "NAME"=>"Левый блок раздела"
    ));
    ?>
</div>
<div class="ts-col-md-16">

<?
if (!empty($arResult['VARIABLES']['ELEMENT_CODE'])) {
    $objFindTools = new CIBlockFindTools();
    $elementID = $objFindTools->GetElementID(false, $arResult['VARIABLES']['ELEMENT_CODE'], false, false, array("IBLOCK_ID" => $arParams['IBLOCK_ID']));

    if (!empty($elementID)) {
        $APPLICATION->IncludeComponent("travelsoft:travelsoft.news.list", "articles_1", Array(
	"ACTIVE_DATE_FORMAT" => "d.m.Y",	// Формат показа даты
		"ADD_SECTIONS_CHAIN" => "N",	// Включать раздел в цепочку навигации
		"AFP_123" => "",	// Автор
		"AFP_228" => "",	// Разрешить комментарий
		"AFP_286" => "",	// Не показывать большую картинку на детальной странице
		"AFP_290" => "",
		"AFP_291" => "",
		"AFP_292" => "",
		"AFP_295" => "",
		"AFP_296" => "",
		"AFP_297" => "",
		"AFP_298" => "",
		"AFP_5" => "",	// Топ-новость раздела
		"AFP_6" => "",	// Топ-новость на главной
		"AFP_67" => "",	// Страна
		"AFP_68" => "",	// Курорт
		"AFP_7" => array(	// Тема
			0 => $elementID,
		),
		"AFP_ID" => "",	// ID элемента(ов)(если несколько значений, то указывать через запятую)
		"AFP_MAX_2" => "",	// Количество комментариев к элементу(максимальное значение)
		"AFP_MAX_259" => "",	// old_Газета (максимальное значение)
		"AFP_MAX_287" => "",
		"AFP_MAX_288" => "",
		"AFP_MAX_3" => "",	// Тема форума для комментариев(максимальное значение)
		"AFP_MAX_56" => "",	// Сумма оценок(максимальное значение)
		"AFP_MAX_57" => "",	// Количество проголосовавших(максимальное значение)
		"AFP_MAX_58" => "",	// Рейтинг(максимальное значение)
		"AFP_MIN_2" => "",	// Количество комментариев к элементу(минимальное значение)
		"AFP_MIN_259" => "",	// old_Газета (минимальное значение)
		"AFP_MIN_287" => "",
		"AFP_MIN_288" => "",
		"AFP_MIN_3" => "",	// Тема форума для комментариев(минимальное значение)
		"AFP_MIN_56" => "",	// Сумма оценок(минимальное значение)
		"AFP_MIN_57" => "",	// Количество проголосовавших(минимальное значение)
		"AFP_MIN_58" => "",	// Рейтинг(минимальное значение)
		"AJAX_MODE" => "N",	// Включить режим AJAX
		"AJAX_OPTION_ADDITIONAL" => "",	// Дополнительный идентификатор
		"AJAX_OPTION_HISTORY" => "N",	// Включить эмуляцию навигации браузера
		"AJAX_OPTION_JUMP" => "N",	// Включить прокрутку к началу компонента
		"AJAX_OPTION_STYLE" => "Y",	// Включить подгрузку стилей
		"CACHE_FILTER" => "N",	// Кешировать при установленном фильтре
		"CACHE_GROUPS" => "Y",	// Учитывать права доступа
		"CACHE_TIME" => "36000000",	// Время кеширования (сек.)
		"CACHE_TYPE" => "A",	// Тип кеширования
		"CHECK_DATES" => "Y",	// Показывать только активные на данный момент элементы
		"COMPOSITE_FRAME_MODE" => "A",	// Голосование шаблона компонента по умолчанию
		"COMPOSITE_FRAME_TYPE" => "AUTO",	// Содержимое компонента
		"DETAIL_URL" => "",	// URL страницы детального просмотра (по умолчанию - из настроек инфоблока)
		"DISPLAY_BOTTOM_PAGER" => "Y",	// Выводить под списком
		"DISPLAY_DATE" => "N",
		"DISPLAY_NAME" => "N",
		"DISPLAY_PICTURE" => "N",
		"DISPLAY_PREVIEW_TEXT" => "N",
		"DISPLAY_TOP_PAGER" => "N",	// Выводить над списком
		"FIELD_CODE" => array(	// Поля
			0 => "",
			1 => "",
		),
		"FILTER_NAME" => "",	// Фильтр
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",	// Скрывать ссылку, если нет детального описания
		"IBLOCK_ID" => "2",	// Код информационного блока
		"IBLOCK_TYPE" => "news",	// Тип информационного блока (используется только для проверки)
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",	// Включать инфоблок в цепочку навигации
		"INCLUDE_SUBSECTIONS" => "N",	// Показывать элементы подразделов раздела
		"MESSAGE_404" => "",	// Сообщение для показа (по умолчанию из компонента)
		"NEWS_COUNT" => "20",	// Количество новостей на странице
		"PAGER_BASE_LINK_ENABLE" => "N",	// Включить обработку ссылок
		"PAGER_DESC_NUMBERING" => "N",	// Использовать обратную навигацию
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",	// Время кеширования страниц для обратной навигации
		"PAGER_SHOW_ALL" => "N",	// Показывать ссылку "Все"
		"PAGER_SHOW_ALWAYS" => "N",	// Выводить всегда
		"PAGER_TEMPLATE" => "main",	// Шаблон постраничной навигации
		"PAGER_TITLE" => "Новости",	// Название категорий
		"PARENT_SECTION" => "",	// ID раздела
		"PARENT_SECTION_CODE" => "",	// Код раздела
		"PREVIEW_TRUNCATE_LEN" => "",	// Максимальная длина анонса для вывода (только для типа текст)
		"PROPERTY_CODE" => array(	// Свойства
			0 => "PHOTO3",
			1 => "MORE_PHOTO",
			2 => "AUTHOR",
			4 => "",
		),
		"SET_BROWSER_TITLE" => "N",	// Устанавливать заголовок окна браузера
		"SET_LAST_MODIFIED" => "N",	// Устанавливать в заголовках ответа время модификации страницы
		"SET_META_DESCRIPTION" => "N",	// Устанавливать описание страницы
		"SET_META_KEYWORDS" => "N",	// Устанавливать ключевые слова страницы
		"SET_STATUS_404" => "N",	// Устанавливать статус 404
		"SET_TITLE" => "N",	// Устанавливать заголовок страницы
		"SHOW_404" => "N",	// Показ специальной страницы
		"SORT_BY1" => "ACTIVE_FROM",	// Поле для первой сортировки новостей
		"SORT_BY2" => "SORT",	// Поле для второй сортировки новостей
		"SORT_ORDER1" => "DESC",	// Направление для первой сортировки новостей
		"SORT_ORDER2" => "ASC",	// Направление для второй сортировки новостей
		"COMPONENT_TEMPLATE" => "articles_",
		"TEXT_TITLE" => "",	// Заголовок блока
		"TEXT_DESCRIPTION" => "",	// Строка описания блока
		"DESCRIPTION_LINK" => "",	// Ссылка описания блока
	),
	false
);
    }
}
?>
</div>
</div>
</div>
</div>