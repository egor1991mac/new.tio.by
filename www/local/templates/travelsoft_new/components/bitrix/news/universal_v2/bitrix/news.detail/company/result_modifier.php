<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

if(!empty($arResult["DISPLAY_PROPERTIES"]["AUTHOR"]["VALUE"])){

    $rsUser = CUser::GetByID($arResult["DISPLAY_PROPERTIES"]["AUTHOR"]["VALUE"]);
    $arUser = $rsUser->Fetch();
    $arResult["DISPLAY_PROPERTIES"]["AUTHOR"]["DESC"] = $arUser["NAME"]." ".$arUser["LAST_NAME"];

}

if(!empty($arResult["DISPLAY_PROPERTIES"]["MORE_PHOTO"]["VALUE"]) && count($arResult["DISPLAY_PROPERTIES"]["MORE_PHOTO"]["VALUE"]) > 1){

    $arResult["SLIDER1"] =  getSrc($arResult["DISPLAY_PROPERTIES"]["MORE_PHOTO"]["VALUE"], array('width' => 740, 'height' => 533));

}

if(!empty($arResult["DISPLAY_PROPERTIES"]["MORE_PHOTO2"]["VALUE"]) && count($arResult["DISPLAY_PROPERTIES"]["MORE_PHOTO2"]["VALUE"]) > 1){

    $arResult["SLIDER2"] =  getSrc($arResult["DISPLAY_PROPERTIES"]["MORE_PHOTO2"]["VALUE"], array('width' => 740, 'height' => 533));

}

$res = CIBlockElement::GetList(Array(), Array("IBLOCK_ID" => $arResult['IBLOCK_ID'], 'PROPERTY_MAIN_OFFICE' => $arResult['ID'], "ACTIVE" => "Y"), false, false, Array("IBLOCK_ID", "ID", "NAME"));
while ($ob = $res->GetNextElement()) {
    $arFields = $ob->GetFields();
    $arProps = $ob->GetProperties();

    if($arResult['ID'] != $arFields['ID']) {

        $arResult['OFFICES']['DATA'][$arFields['ID']] = [
            'ADDRESS' => $arProps['ADDRESS']['VALUE'],
            'PHONE' => $arProps['PHONE']['VALUE'],
        ];

        if (!empty($arProps['MAP']['VALUE'])) {
            $coordinates = explode(',', $arProps['MAP']['VALUE']);
            $arResult['OFFICES']['MAP'][$arFields['ID']] = [
                'lat' => $coordinates[0],
                'lng' => $coordinates[1],
                'title' => $arFields['NAME']
            ];
        }

    }
}
if(!empty($arResult['DISPLAY_PROPERTIES']['MAP']['VALUE'])){
    $coordinates = explode(',', $arResult['DISPLAY_PROPERTIES']['MAP']['VALUE']);
    $arResult['MAP'] = [
        'lat' => $coordinates[0],
        'lng' => $coordinates[1],
        'title' => $arResult['NAME']
    ];
}