window.YandexMapFunctionsContainer = {

    // markers options
    _markersOptions: [],

    // initialized markers
    _markers: [],

    // google map object
    _map: null

};


createMarker = function (marker) {
    var placeMark = new ymaps.Placemark([marker.lat, marker.lng],
        {
            // Хинт показывается при наведении мышкой на иконку метки.
            hintContent: marker.title,
            // Балун откроется при клике по метке.
            balloonContent: marker.title
        });

    window.YandexMapFunctionsContainer._map.geoObjects.add(placeMark);
    window.YandexMapFunctionsContainer._markers.push(placeMark);
};

window.YandexMapFunctionsContainer.createMarkers = function (markers) {
    console.log(markers);
    /*
    markers.forEach(function (marker, index) {
        window.YandexMapFunctionsContainer.createMarker(marker);
    });
    return window.YandexMapFunctionsContainer._map;*/
};

window.YandexMapFunctionsContainer.createMap = function (idSelector, route_info) {

    ymaps.ready(
        function () {

            window.YandexMapFunctionsContainer._map = new ymaps.Map(idSelector, {
                center: [route_info.lat, route_info.lng],
                zoom: route_info.zoom
            });
            createMarker({lat: route_info.lat, lng: route_info.lng, title: route_info.title});

            return window.YandexMapFunctionsContainer._map;
        }
    );
};