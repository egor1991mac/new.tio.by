<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
//$this->addExternalCss(SITE_TEMPLATE_PATH . "/assets/plugins/slider-pro/slider-pro.css");
//$this->addExternalJS(SITE_TEMPLATE_PATH . "/assets/plugins/slider-pro/jquery.sliderPro.min.owl");

$img = getSrc($arResult["DISPLAY_PROPERTIES"]["PHOTO"]["VALUE"], array('width' => 1140, 'height' => 400));
if (empty($img[0])) {
    $img = getSrc($arResult["DISPLAY_PROPERTIES"]["PHOTO"]["VALUE"], array('width' => 1140, 'height' => 400));
}
?>

<section class="b-post b-post_img-bg b-post_img-bg_type-8 clearfix">
    <div class="entry-media">
        <? if (!empty($img[0])): ?>
            <img class="img-responsive" src="<?= $img[0] ?>" alt="<?= $arResult["NAME"] ?>">
        <? else: ?>
            <img class="img-responsive" src="/images/agent-img.jpg" alt="<?= $arResult["NAME"] ?>">
        <? endif ?>
    </div>
    <div class="entry-main">
        <div class="entry-header">
            <div class="entry-meta">
                <? if (!empty($arResult["PROPERTIES"]["LOGO"]["VALUE"])): ?>
                    <? $file_big = CFile::ResizeImageGet($arResult["PROPERTIES"]["LOGO"]["VALUE"], Array('width' => 270, 'height' => 100), BX_RESIZE_IMAGE_PROPORTIONAL_ALT, true); ?>
                    <img src="<?= $file_big["src"]; ?>" class="attachment- size- wp-post-image"
                         alt="Логотип <?= $arResult['NAME'] ?>"/>
                <? endif; ?>
                <? if (!empty($arResult["PROPERTIES"]["COUNTRY"]['VALUE'])): ?>
                    <span class="entry-meta__item">
                        <?= $arResult["DISPALY_PROPERTIES"]["COUNTRY"]['DISPALY_VALUE']; ?>
                    </span>
                <? endif ?>
                <? if (!empty($arResult["PROPERTIES"]["CITY"]['VALUE'])): ?>
                    <span class="entry-meta__item">
                        <?= $arResult["DISPALY_PROPERTIES"]["CITY"]['DISPALY_VALUE']; ?>
                    </span>
                <? endif ?>
                <? if (!empty($arResult["PROPERTIES"]["UNP"]['VALUE'])): ?>
                    <span class="entry-meta__item">
                        <?= strip_tags($arResult["PROPERTIES"]["UNP"]["VALUE"]) ?>
                    </span>
                <? endif ?>
                <? if (is_array($arResult["PROPERTIES"]["TYPE"]['VALUE'])): ?><span
                        class="entry-label bg-second"><?= implode(', ', (array)$arResult["PROPERTIES"]["TYPE"]['VALUE']) ?></span><? endif; ?>
                <? if (in_array(array(1, 7), $GLOBALS["USER"]->GetUserGroupArray())): ?>
                    <span class="entry-meta__item"><i class="icon fa fa-heart-o text-second"></i>
                        <a class="entry-meta__link"><?= $arResult["SHOW_COUNTER"] ?></a>
                    </span>
                <? endif ?>
            </div>
            <h1 class="entry-title hov-title">
                <a class="hov-title__inner"><?= $arResult["NAME"] ?></a>
            </h1>
        </div>
    </div>
</section>
<div class="col-md-9">
    <div class="l-main-content">
        <div class="row">
            <? if (!empty($arResult['MAP'])): ?>
                <script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
                <div id="map" style="width: 100%; height: 400px"></div>
                <script type="text/javascript">
                    // Функция ymaps.ready() будет вызвана, когда
                    // загрузятся все компоненты API, а также когда будет готово DOM-дерево.
                    ymaps.ready(init);

                    function init() {
                        // Создание карты.
                        var myMap = new ymaps.Map("map", {
                            // Координаты центра карты.
                            // Порядок по умолчнию: «широта, долгота».
                            // Чтобы не определять координаты центра карты вручную,
                            // воспользуйтесь инструментом Определение координат.
                            center: [<?=$arResult['MAP']['lat']?>, <?=$arResult['MAP']['lng']?>],
                            // Уровень масштабирования. Допустимые значения:
                            // от 0 (весь мир) до 19.
                            zoom: 13
                        });

                        var placeMark = new ymaps.Placemark([<?=$arResult['MAP']['lat']?>, <?=$arResult['MAP']['lng']?>], {
                            // Хинт показывается при наведении мышкой на иконку метки.
                            hintContent: "<?=$arResult['MAP']['title']?>",
                            // Балун откроется при клике по метке.
                            balloonContent: "<?=$arResult['MAP']['title']?>"
                        });
                        myMap.geoObjects.add(placeMark);

                        <?if(!empty($arResult['OFFICES']['MAP'])):?>

                        <?foreach ($arResult['OFFICES']['MAP'] as $office):?>

                        placeMark = new ymaps.Placemark([<?=$office['lat']?>, <?=$office['lng']?>], {
                            // Хинт показывается при наведении мышкой на иконку метки.
                            hintContent: "<?=$office['title']?>",
                            // Балун откроется при клике по метке.
                            balloonContent: "<?=$office['title']?>"
                        });
                        myMap.geoObjects.add(placeMark);

                        <?endforeach;?>

                        <?endif;?>
                    }
                </script>
            <? endif; ?>
            <? /*
            $places = "{i:0;a:3:{s:3:\"LON\";d:27.547366580967022;s:3:\"LAT\";d:53.916169165021145;s:4:\"TEXT\";s:16:\"Подпись 1\";}i:1;a:3:{s:3:\"LON\";d:27.602449597922426;s:3:\"LAT\";d:53.912136162276866;s:4:\"TEXT\";s:16:\"Подпись 2\";}}";
            $str = "a:4:{s:10:\"yandex_lat\";d:53.896403269593264;s:10:\"yandex_lon\";d:27.53397699356469;s:12:\"yandex_scale\";i:12;s:10:\"PLACEMARKS\";a:2:" . $places . "}";
            ?>

            <? $APPLICATION->IncludeComponent(
                "bitrix:map.yandex.view",
                "",
                Array(
                    "COMPOSITE_FRAME_MODE" => "A",
                    "COMPOSITE_FRAME_TYPE" => "AUTO",
                    "CONTROLS" => array("ZOOM", "MINIMAP", "SCALELINE"),
                    "INIT_MAP_TYPE" => "MAP",
                    "MAP_DATA" => $str,
                    "MAP_HEIGHT" => "500",
                    "MAP_ID" => "company-map",
                    "MAP_WIDTH" => "100%",
                    "OPTIONS" => array("ENABLE_SCROLL_ZOOM", "ENABLE_DBLCLICK_ZOOM", "ENABLE_DRAGGING")
                )
            ); */ ?>
        </div>
        <article class="b-post b-post-full clearfix">
            <div class="entry-main">

                <? ob_start() ?>

                <? if (!empty($arResult["PROPERTIES"]["UNP"]["VALUE"])): ?>
                    <p>УНП: <?= strip_tags($arResult["PROPERTIES"]["UNP"]["VALUE"]) ?>
                    </p>
                <? endif; ?>
                <? if (!empty($arResult["PROPERTIES"]["ADDRESS"]["VALUE"])): ?><h5>Адрес</h5><? endif; ?>
                <? if (!empty($arResult["PROPERTIES"]["CITY"]["VALUE"])): ?>
                    <p><?= strip_tags($arResult["DISPLAY_PROPERTIES"]["CITY"]["DISPLAY_VALUE"]) ?><? if (!empty($arResult["PROPERTIES"]["METRO"]["VALUE"])): ?>, ст. метро <?= strip_tags($arResult["DISPLAY_PROPERTIES"]["METRO"]["DISPLAY_VALUE"]) ?><? endif; ?>
                    </p>
                <? endif; ?>
                <? if (!empty($arResult["PROPERTIES"]["ADDRESS"]["VALUE"])): ?>
                    <p>
                        <i class="fa fa-map-marker fa-2"></i> <?= strip_tags($arResult["PROPERTIES"]["ADDRESS"]["VALUE"]) ?>
                    </p>
                <? endif; ?>
                <? if (!empty($arResult["PROPERTIES"]["EMAIL"]["VALUE"])): ?>
                    <p><i class="fa fa-envelope fa-2"></i> <a
                                href="mailto:<?= strip_tags($arResult["PROPERTIES"]["EMAIL"]["VALUE"]) ?>"><?= strip_tags($arResult["PROPERTIES"]["EMAIL"]["VALUE"]) ?></a>
                    </p>
                <? endif; ?>
                <? if (!empty($arResult["PROPERTIES"]["FACEBOOK"]["VALUE"])): ?>
                    <p><i class="fa fa-facebook-official fa-2"></i> <a
                                href="<?= strip_tags($arResult["PROPERTIES"]["FACEBOOK"]["VALUE"]) ?>"
                                target="_blank"><?= (substr(strip_tags($arResult["PROPERTIES"]["FACEBOOK"]["VALUE"]), 0, 32)) ?>
                            ...</a>
                    </p>
                <? endif; ?>
                <? if (!empty($arResult["PROPERTIES"]["VK"]["VALUE"])): ?>
                    <p><i class="fa fa-vk fa-2"></i> <a href="<?= strip_tags($arResult["PROPERTIES"]["VK"]["VALUE"]) ?>"
                                                        target="_blank"><?= strip_tags($arResult["PROPERTIES"]["VK"]["VALUE"]) ?></a>
                    </p>
                <? endif; ?>
                <? if (!empty($arResult["PROPERTIES"]["SKYPE"]["VALUE"])): ?>
                    <p><i class="fa fa-skype fa-2"></i> <a
                                href="call:<?= strip_tags($arResult["PROPERTIES"]["SKYPE"]["VALUE"]) ?>"
                                target="_blank"><?= strip_tags($arResult["PROPERTIES"]["SKYPE"]["VALUE"]) ?></a>
                    </p>
                <? endif; ?>
                <? if (!empty($arResult["PROPERTIES"]["SITE"]["VALUE"])): ?>
                    <p><i class="fa fa-sitemap fa-2"></i> <a
                                href="/frame/?url=<?= strip_tags($arResult["PROPERTIES"]["SITE"]["VALUE"]) ?>"
                                target="_blank"><?= strip_tags($arResult["PROPERTIES"]["SITE"]["VALUE"]) ?></a>
                    </p>
                <? endif; ?>
                <? if (!empty($arResult["PROPERTIES"]["PHONE"]["VALUE"][0])): ?>
                    <h5>Телефон</h5>
                    <p><i class="fa fa-phone-square fa-2"></i> <a
                                href="tel:<?= strip_tags($arResult["PROPERTIES"]["PHONE"]["VALUE"][0]) ?>"><?= strip_tags($arResult["PROPERTIES"]["PHONE"]["VALUE"][0]) ?></a>
                    </p>
                <? endif; ?>
                <? if (!empty($arResult["PROPERTIES"]["PHONE"]["VALUE"][1])): ?>
                    <p><i class="fa fa-phone-square fa-2"></i> <a
                                href="tel:<?= strip_tags($arResult["PROPERTIES"]["PHONE"]["VALUE"][1]) ?>"><?= strip_tags($arResult["PROPERTIES"]["PHONE"]["VALUE"][1]) ?></a>
                    </p>
                <? endif; ?>
                <? if (!empty($arResult["PROPERTIES"]["PHONE"]["VALUE"][2])): ?>
                    <p><i class="fa fa-phone-square fa-2"></i> <a
                                href="tel:<?= strip_tags($arResult["PROPERTIES"]["PHONE"]["VALUE"][2]) ?>"><?= strip_tags($arResult["PROPERTIES"]["PHONE"]["VALUE"][2]) ?></a>
                    </p>
                <? endif; ?>
                <? if (!empty($arResult["PROPERTIES"]["PHONE"]["VALUE"][3])): ?>
                    <p><i class="fa fa-phone-square fa-2"></i> <a
                                href="tel:<?= strip_tags($arResult["PROPERTIES"]["PHONE"]["VALUE"][3]) ?>"><?= strip_tags($arResult["PROPERTIES"]["PHONE"]["VALUE"][3]) ?></a>
                    </p>
                <? endif; ?>
                <? if (!empty($arResult["PROPERTIES"]["WORKING_HOURS"]["VALUE"])): ?>
                    <h5>Время работы</h5><p><?= strip_tags($arResult["PROPERTIES"]["WORKING_HOURS"]["VALUE"]) ?></p>
                <? endif; ?>

                <? $GLOBALS['TIO']['SIDEBAR'] = ob_get_clean(); ?>

                <? if (!empty($arResult["PROPERTIES"]["ABOUT"]['VALUE'])): ?>
                    <div class="mvp-related-posts left relative">
                        <h4 class="ui-title-inner"><span class="ui-title-inner__inner">О компании</span></h4>
                        <?= htmlspecialcharsBack($arResult["PROPERTIES"]["ABOUT"]["VALUE"]["TEXT"]) ?>
                    </div>
                <? endif; ?>
                <? if (strlen($arResult["DETAIL_TEXT"]) > 0): ?>
                    <? echo $arResult["DETAIL_TEXT"]; ?>
                <? else: ?>
                    <? echo $arResult["PREVIEW_TEXT"]; ?>
                <? endif ?>
                <? if (!empty($arResult["DISPLAY_PROPERTIES"]["PHOTO"]["VALUE"])): ?>
            </div>
        </article>
        <div class="row">
            <div class="main-slider row" id="main-slider1" data-slider-width="100%"
                 data-slider-height="550px" data-slider-arrows="false" data-slider-buttons="false"
                 style="margin: 0">
                <div class="sp-slides">
                    <? $images = getSrc($arResult["DISPLAY_PROPERTIES"]["PHOTO"]["VALUE"], array('width' => 840, 'height' => 497), NO_PHOTO_PATH_840_497) ?>
                    <? foreach ($images as $k => $slide): ?>
                        <? $slide_desc = $arResult["DISPLAY_PROPERTIES"]["PHOTO"]["DESCRIPTION"][$k]; ?>
                        <!-- Slide -->
                        <div class="sp-slide">
                            <img class="sp-image" src="<?= $slide ?>"
                                 alt="<?= !empty($slide_desc) ? $slide_desc : 'slide-' . ($k + 1) ?>"/>
                            <? if (!empty($slide_desc)): ?>
                                <div class="main-slider__info sp-layer" data-width="100%"
                                     data-show-transition="left" data-hide-transition="left"
                                     data-show-duration="2000" data-show-delay="1200" data-hide-delay="400">
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-md-10">
                                                <div class="photo-title"><?= $slide_desc ?></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <? endif ?>
                        </div>
                    <? endforeach; ?>
                </div>
            </div>
        </div>
        <article class="b-post b-post-full clearfix">
            <div class="entry-main">
                <? endif ?>

                <? if (!empty($arResult["DISPLAY_PROPERTIES"]["DESC1"]["VALUE"]["TEXT"]) || !empty($arResult["DISPLAY_PROPERTIES"]["DESC2"]["VALUE"]["TEXT"]) || !empty($arResult["DISPLAY_PROPERTIES"]["DESC3"]["VALUE"]["TEXT"])): ?>
                    <? if (!empty($arResult["DISPLAY_PROPERTIES"]["DESC1"]["VALUE"]["TEXT"])): ?>
                        <div class="entry-content">
                            <p><?= $arResult["DISPLAY_PROPERTIES"]["DESC1"]["DISPLAY_VALUE"] ?></p>
                        </div>
                        <? if (isset($arResult["SLIDER1"]) && !empty($arResult["SLIDER1"])): ?>
                            <div class="main-slider slider-pro" id="main-slider1" data-slider-width="78%"
                                 data-slider-height="550px" data-slider-arrows="false" data-slider-buttons="false">
                                <div class="sp-slides">
                                    <? foreach ($arResult["SLIDER1"] as $k => $slide): ?>
                                        <? $slide_desc = $arResult["DISPLAY_PROPERTIES"]["MORE_PHOTO"]["DESCRIPTION"][$k]; ?>
                                        <!-- Slide -->
                                        <div class="sp-slide">
                                            <img class="sp-image" src="<?= $slide ?>"
                                                 alt="<?= !empty($slide_desc) ? $slide_desc : 'slide-' . $k + 1 ?>"/>
                                            <? if (!empty($slide_desc)): ?>
                                                <div class="main-slider__info sp-layer" data-width="100%"
                                                     data-show-transition="left" data-hide-transition="left"
                                                     data-show-duration="2000" data-show-delay="1200"
                                                     data-hide-delay="400">
                                                    <div class="container">
                                                        <div class="row">
                                                            <div class="col-md-10">
                                                                <section class="b-post b-post_slider clearfix">
                                                                    <div class="entry-main">
                                                                        <div class="entry-header">
                                                                            <h2 class="entry-title hov-title"><a
                                                                                        class="hov-title__inner"><?= $slide_desc ?></a>
                                                                            </h2>
                                                                        </div>
                                                                    </div>
                                                                </section>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            <? endif ?>
                                        </div>
                                    <? endforeach; ?>
                                </div>
                            </div>
                        <? endif ?>
                    <? endif ?>
                    <? if (!empty($arResult["DISPLAY_PROPERTIES"]["DESC2"]["VALUE"]["TEXT"])): ?>
                        <div class="entry-content">
                            <p><?= $arResult["DISPLAY_PROPERTIES"]["DESC2"]["DISPLAY_VALUE"] ?></p>
                        </div>
                        <? if (isset($arResult["SLIDER2"]) && !empty($arResult["SLIDER2"])): ?>
                            <div class="main-slider slider-pro" id="main-slider2" data-slider-width="78%"
                                 data-slider-height="550px" data-slider-arrows="false" data-slider-buttons="false">
                                <div class="sp-slides">
                                    <? foreach ($arResult["SLIDER2"] as $k => $slide): ?>
                                        <? $slide_desc = $arResult["DISPLAY_PROPERTIES"]["MORE_PHOTO2"]["DESCRIPTION"][$k]; ?>
                                        <!-- Slide -->
                                        <div class="sp-slide">
                                            <img class="sp-image" src="<?= $slide ?>"
                                                 alt="<?= !empty($slide_desc) ? $slide_desc : 'slide-' . $k + 1 ?>"/>
                                            <? if (!empty($slide_desc)): ?>
                                                <div class="main-slider__info sp-layer" data-width="100%"
                                                     data-show-transition="left" data-hide-transition="left"
                                                     data-show-duration="2000" data-show-delay="1200"
                                                     data-hide-delay="400">
                                                    <div class="container">
                                                        <div class="row">
                                                            <div class="col-md-10">
                                                                <section class="b-post b-post_slider clearfix">
                                                                    <div class="entry-main">
                                                                        <div class="entry-header">
                                                                            <h2 class="entry-title hov-title"><a
                                                                                        class="hov-title__inner"><?= $slide_desc ?></a>
                                                                            </h2>
                                                                        </div>
                                                                    </div>
                                                                </section>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            <? endif ?>
                                        </div>
                                    <? endforeach; ?>
                                </div>
                            </div>
                        <? endif ?>
                    <? endif ?>
                    <? if (!empty($arResult["DISPLAY_PROPERTIES"]["DESC3"]["VALUE"]["TEXT"])): ?>
                        <div class="entry-content">
                            <p><?= $arResult["DISPLAY_PROPERTIES"]["DESC3"]["DISPLAY_VALUE"] ?></p>
                        </div>
                    <? endif ?>
                <? else: ?>
                    <div class="entry-content">
                        <p><?= $arResult["~DETAIL_TEXT"] ?></p>
                        <? if (!empty($arResult['DISPLAY_PROPERTIES']['DESC']['~VALUE']['TEXT'])): ?>
                            <p><?= $arResult['DISPLAY_PROPERTIES']['DESC']['~VALUE']['TEXT'] ?></p>
                        <? endif; ?>
                        <br>
                        <? if (isset($arResult["PHOTO"]) && !empty($arResult["PHOTO"])): ?>
                            <div class="main-slider slider-pro" id="main-slider1" data-slider-width="78%"
                                 data-slider-height="550px" data-slider-arrows="false" data-slider-buttons="false"
                                 style="margin: 0">
                                <div class="sp-slides">
                                    <? foreach ($arResult["SLIDER1"] as $k => $slide): ?>
                                        <? $slide_desc = $arResult["DISPLAY_PROPERTIES"]["PHOTO"]["DESCRIPTION"][$k]; ?>
                                        <!-- Slide -->
                                        <div class="sp-slide">
                                            <img class="sp-image" src="<?= $slide ?>"
                                                 alt="<?= !empty($slide_desc) ? $slide_desc : 'slide-' . $k + 1 ?>"/>
                                            <? if (!empty($slide_desc)): ?>
                                                <div class="main-slider__info sp-layer" data-width="100%"
                                                     data-show-transition="left" data-hide-transition="left"
                                                     data-show-duration="2000" data-show-delay="1200"
                                                     data-hide-delay="400">
                                                    <div class="container">
                                                        <div class="row">
                                                            <div class="col-md-10">
                                                                <section class="b-post b-post_slider clearfix">
                                                                    <div class="entry-main">
                                                                        <div class="entry-header">
                                                                            <h2 class="entry-title hov-title"><a
                                                                                        class="hov-title__inner"><?= $slide_desc ?></a>
                                                                            </h2>
                                                                        </div>
                                                                    </div>
                                                                </section>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            <? endif ?>
                                        </div>
                                    <? endforeach; ?>
                                </div>
                            </div>
                        <? endif ?>
                    </div>
                <? endif ?>
                <div class="entry-footer">
                    <? if (!empty($arResult["DISPLAY_PROPERTIES"]["THEME"]["VALUE"])): ?>
                        <div class="post-tags"><span class="entry-footer__title"><?= GetMessage('THEME') ?></span>
                            <ul class="list-tags list-tags_grey list-unstyled">
                                <? if (isset($arResult["DISPLAY_PROPERTIES"]["THEME"]["LINK_ELEMENT_VALUE"]) && !empty($arResult["DISPLAY_PROPERTIES"]["THEME"]["LINK_ELEMENT_VALUE"])): ?>
                                    <? foreach ($arResult["DISPLAY_PROPERTIES"]["THEME"]["LINK_ELEMENT_VALUE"] as $theme): ?>
                                        <li class="list-tags__item"><a class="list-tags__link btn btn-default"
                                                                       href="<?= $theme["DETAIL_PAGE_URL"] ?>"><?= $theme["NAME"] ?></a>
                                        </li>
                                    <? endforeach; ?>
                                <? else: ?>
                                    <? if (is_array($arResult["DISPLAY_PROPERTIES"]["THEME"]["DISPLAY_VALUE"])): ?>
                                        <?= implode2('</li>, <li class="list-tags__item">', $arResult["DISPLAY_PROPERTIES"]["THEME"]["DISPLAY_VALUE"]) ?>
                                    <? else: ?>
                                        <li class="list-tags__item"><?= $arResult["DISPLAY_PROPERTIES"]["THEME"]["DISPLAY_VALUE"] ?></li>
                                    <? endif ?>
                                <? endif ?>
                            </ul>
                        </div>
                    <? endif ?>
                    <div class="entry-footer__social">
                        <? $APPLICATION->IncludeComponent(
                            "bitrix:main.share",
                            "main",
                            Array(
                                "HANDLERS" => array("facebook", "twitter", "vk"),
                                "HIDE" => "N",
                                "PAGE_TITLE" => $APPLICATION->GetTitle(),
                                "PAGE_URL" => $_SERVER["REQUEST_URI"],
                                "SHORTEN_URL_KEY" => "",
                                "SHORTEN_URL_LOGIN" => ""
                            )
                        ); ?>
                    </div>
                </div>
            </div>
        </article>
        <!-- end .post-->
    </div>
</div>
<script>
    $(document).ready(function () {
        $(".entry-content img").each(function () {
                $(this).addClass('img-responsive');
            }
        );
        $(".entry-footer .list-tags li").each(function () {
                if ($(this).find('a').hasClass('list-tags__link') === false) {
                    $(this).find('a').addClass('list-tags__link btn btn-default');
                }
            }
        );
    });
</script>