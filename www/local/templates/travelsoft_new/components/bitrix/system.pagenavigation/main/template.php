<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if(!$arResult["NavShowAlways"])
{
    if ($arResult["NavRecordCount"] == 0 || ($arResult["NavPageCount"] == 1 && $arResult["NavShowAll"] == false))
        return;
}
?>

<ul class="pagination text-center">

    <?
    $strNavQueryString = ($arResult["NavQueryString"] != "" ? $arResult["NavQueryString"]."&amp;" : "");
    $strNavQueryStringFull = ($arResult["NavQueryString"] != "" ? "?".$arResult["NavQueryString"] : "");
    ?>
    <?$StartPage = true;?>
    <?$EndPage = true;?>

    <?if($arResult["NavPageNomer"] == "1"):?>
        <li><a><i class="icon angle-left"></i></a></li>
    <?else:?>
        <li><a href="<?=$arResult["sUrlPathParams"]?>PAGEN_<?=$arResult["NavNum"]?>=<?=($arResult["NavPageNomer"]-1)?>"><i class="icon angle-left"></i></a></li>
    <?endif;?>

    <?for($page = 1; $page <= $arResult["NavPageCount"]; $page++):?>
        <?if($arResult["NavPageNomer"] == $page):?>
            <li class="active">
                <a><?=$page?></a>
            </li>
        <?else:?>
            <?if($page == "1"):?>
                <li>
                    <a href="<?=$arResult["sUrlPathParams"]?>PAGEN_<?=$arResult["NavNum"]?>=<?=$page;?>"><?=$page?></a>
                </li>
            <?elseif($page == $arResult["NavPageCount"]):?>
                <li>
                    <a href="<?=$arResult["sUrlPathParams"]?>PAGEN_<?=$arResult["NavNum"]?>=<?=$page;?>"><?=$page?></a>
                </li>
            <?elseif(($arResult["nStartPage"] > $page) && (($arResult["nStartPage"] - 2) != "1")):?>
                <?if($StartPage):?>
                    <li>
                        <a>...</a>
                    </li>
                    <?$StartPage = false;?>
                <?endif;?>
            <?elseif(($arResult["nEndPage"] < $page) && (($arResult["nEndPage"] + 2) != $arResult["NavPageCount"])):?>
                <?if($EndPage):?>
                    <li>
                        <a>...</a>
                    </li>
                    <?$EndPage = false;?>
                <?endif;?>
            <?else:?>
                <li>
                    <a href="<?=$arResult["sUrlPathParams"]?>PAGEN_<?=$arResult["NavNum"]?>=<?=$page;?>"><?=$page?></a>
                </li>
            <?endif;?>
        <?endif;?>
    <?endfor;?>

    <?if($arResult["NavPageNomer"] == $arResult["NavPageCount"]):?>
        <li><a><i class="icon angle-right"></i></a></li>
    <?else:?>
        <li><a href="<?=$arResult["sUrlPathParams"]?>PAGEN_<?=$arResult["NavNum"]?>=<?=($arResult["NavPageNomer"]+1)?>"><i class="icon angle-right"></i></a></li>
    <?endif;?>

</ul>