<?php
/**
 * Created by PhpStorm.
 * User: Sun
 * Date: 27.07.2018
 * Time: 18:03
 */


foreach ($arResult['ITEMS'] as $key => $arItem) {

    $count_news = CIBlockElement::GetList(Array(), Array("IBLOCK_ID" => 2, "PROPERTY_THEME" => $arItem['ID'], "ACTIVE" => "Y"), [], false, Array("IBLOCK_ID", "ID", "NAME"));
    $arResult['ITEMS'][$key]['COUNT_NEWS'] = $count_news;

    $arResult['ITEMS'][$key]["NAME"] = mb_strtoupper_first(trim($arResult['ITEMS'][$key]["NAME"]));

}

$arResult['ITEMS'] = customMultiSort($arResult['ITEMS'], "NAME", false);

foreach ($arResult['ITEMS'] as $key => $arItem) {

    if(empty($arResult['ITEMS'][$key]['COUNT_NEWS'])) {
        unset($arResult['ITEMS'][$key]);
    }

}