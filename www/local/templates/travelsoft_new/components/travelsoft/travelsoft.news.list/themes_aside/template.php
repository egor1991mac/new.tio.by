<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<section class="widget section-sidebar ts-width-100">
    <h2 class="widget-title ui-title-inner ui-title-inner_condensed">
        <span class="ui-title-inner__inner">
            Темы
        </span>

    </h2>
    <div class="widget-content ts-width-100">
        <ul class="widget-list list list-mark-4 ts-m-0">
            <? foreach ($arResult["ITEMS"] as $arItem): ?>
                <? if (!empty($arItem['COUNT_NEWS'])): ?>
                    <?
                    $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                    $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
                    ?>
                    <li class="widget-list__item" id="<?= $this->GetEditAreaId($arItem['ID']); ?>" <?if(strstr($_SERVER["REQUEST_URI"], $arItem["DETAIL_PAGE_URL"])):?>style="font-weight: 700; color: #000" <?endif;?>>
                        <a class="widget-list__link ts-d-flex ts-justify-content__space-between ts-align-items__center" href="<?= $arItem['DETAIL_PAGE_URL'] ?>" style="position: static">
                            <?= $arItem['NAME'] ?>
                            <span class="badge"><?= $arItem['COUNT_NEWS']; ?></span>
                        </a>
                    </li>
                <? endif; ?>
            <? endforeach; ?>
        </ul>
    </div>
</section>
